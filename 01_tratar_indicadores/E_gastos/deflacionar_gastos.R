# IEPS DATA
#
# Esse codigo deflaciona dados de gastos municipais (SIOPS) utilizando o IPCA



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())

# Bibliotecas

xfun::pkg_attach(c('httr', 'rlang', 'rvest', 'dplyr', 'readstata13', 'haven', 'mgsub', 'readxl'), install=T)



# Seta diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos')

# Input
path_siops_mun <-'siops_mun.dta'
path_siops_mun_reg <-'siops_mun_reg.dta'
path_siops_mun_macro <-'siops_mun_macro.dta'
path_siops_mun_uf <-'siops_mun_uf.dta'
path_siops_mun_br <-'siops_mun_br.dta'
path_siops_uf_uf <-'siops_uf_uf.dta'
path_siops_uf_br <-'siops_uf_br.dta'


path_deflator<-'Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/E_gastos/ipca_deflator.xlsx'


# 1. Le arquivos --------------------------------------------------------------------------------------------------------------------------------------------
deflator<-read_excel(path_deflator, sheet = "ipca") %>%
  dplyr::rename(ano = year)%>%
  mutate(ano = as.numeric(as.character((ano))))%>%
  filter(ano >=2010) %>% 
  select(ano, def = def_2022)

siops_mun       <-read_dta(path_siops_mun)
siops_mun_reg   <-read_dta(path_siops_mun_reg)
siops_mun_macro <-read_dta(path_siops_mun_macro)
siops_mun_uf    <-read_dta(path_siops_mun_uf)
siops_mun_br    <-read_dta(path_siops_mun_br)
siops_uf_uf     <-read_dta(path_siops_uf_uf)
siops_uf_br     <-read_dta(path_siops_uf_br)

# 2. Cria variaveis deflacionadas ---------------------------------------------------------------------------------------------------------------------

siops_mun_def<-left_join(siops_mun,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_mun_def = desp_tot_saude_pc_mun*def,
         desp_recp_saude_pc_mun_def = desp_recp_saude_pc_mun*def)%>%
  select(-def)

siops_mun_reg_def<-left_join(siops_mun_reg,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_mun_def = desp_tot_saude_pc_mun*def,
         desp_recp_saude_pc_mun_def = desp_recp_saude_pc_mun*def)%>%
  select(-def)

siops_mun_macro_def<-left_join(siops_mun_macro,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_mun_def = desp_tot_saude_pc_mun*def,
         desp_recp_saude_pc_mun_def = desp_recp_saude_pc_mun*def)%>%
  select(-def)

siops_mun_uf_def<-left_join(siops_mun_uf,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_mun_def = desp_tot_saude_pc_mun*def,
         desp_recp_saude_pc_mun_def = desp_recp_saude_pc_mun*def)%>%
  select(-def)

siops_mun_br_def<-left_join(siops_mun_br,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_mun_def = desp_tot_saude_pc_mun*def,
         desp_recp_saude_pc_mun_def = desp_recp_saude_pc_mun*def)%>%
  select(-def)

siops_uf_uf_def<-left_join(siops_uf_uf,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_uf_def = desp_tot_saude_pc_uf*def,
         desp_recp_saude_pc_uf_def = desp_recp_saude_pc_uf*def)%>%
  select(-def)

siops_uf_br_def<-left_join(siops_uf_br,deflator,by='ano')%>%
  mutate(desp_tot_saude_pc_uf_def = desp_tot_saude_pc_uf*def,
         desp_recp_saude_pc_uf_def = desp_recp_saude_pc_uf*def)%>%
  select(-def)


# 3. Salva e exporta ---------------------------------------------------------------------------------------------------------------------

write_dta(siops_mun_def, 'siops_mun_def.dta')
write_dta(siops_mun_reg_def, 'siops_mun_reg_def.dta')
write_dta(siops_mun_macro_def, 'siops_mun_macro_def.dta')
write_dta(siops_mun_uf_def, 'siops_mun_uf_def.dta')
write_dta(siops_mun_br_def, 'siops_mun_br_def.dta')
write_dta(siops_uf_uf_def, 'siops_uf_uf_def.dta')
write_dta(siops_uf_br_def, 'siops_uf_br_def.dta')
