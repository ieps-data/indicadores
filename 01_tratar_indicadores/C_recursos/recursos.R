
# IEPS DATA

# Esse codigo trata os dados de recursos

# 0. Config -----------------------------------------------------------------------------------------------------------------------------------------

rm(list=ls())
gc()

# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'haven', 'purrr', 'furrr', 'rlang', 'tictoc', 'foreign', 'data.table', 'readxl', 'read.dbc'), install=T)


# Setar diretorio
setwd("Z:/")

# Input
path_lt <- "I:/dados_ieps/dados_brutos/ministerio_da_saude/datasus/cnes/dados/leitos/"
path_pf <- "I:/dados_ieps/dados_brutos/ministerio_da_saude/datasus/cnes/dados/profissional/"
path_dados_prof_cnes_parquet <- "I:/dados_ieps/dados_brutos/ministerio_da_saude/datasus/cnes/dados_parquet/profissional/"
path_reg_saude <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Regioes_2022.dta"
path_macro_saude <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Macrorregioes_2022.dta"
path_pop_idade <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2021.dta"
path_pop_censo <- "Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/G_demografia/pop_idade/pop_censo_2022.xlsx"


# Output
sf_lt_temp <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/lt_cnes_temp.dta"
sf_hr_temp <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/"
sf_mun_temp_1 <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_mun_temp_1.dta"
sf_mun_temp_2 <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_mun_temp_2.dta"
sf_brasil <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_br.dta"
sf_coduf <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_uf.dta"
sf_codmacro <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_macro.dta"
sf_codreg <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_reg.dta"
sf_codmun <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_mun.dta"


# Define lista de anos e meses a serem tratados
meses <- c("1012", "1112", "1212", "1312", "1412", "1512", "1612", "1712", "1812", "1912", "2012", "2112", "2212")


# Prepara dados de populacao -------------------------------------------------------------------------------------------------------------------------
pop_2010_2021 <- haven::read_dta(path_pop_idade) 

# Dados pop 2022 Censo
pop_censo  <-   read_xlsx(path_pop_censo, skip = 4)    %>% 
  rename(codmun = ...1) %>% 
  dplyr::select(-...2, -...3, -...4 ) %>% 
  rename_with(~str_replace(.x, "anos", ""), -codmun) %>% 
  rename_with(~paste0("pop", .x), -codmun) %>% 
  rename_with(~gsub("\\s+", "", .x), everything()) %>% 
  mutate(across(everything(), as.numeric)) %>% 
  rowwise() %>%
  mutate(pop80m = sum(c(pop80a84, pop85a89, pop90a94, pop95a99, pop100oumais), na.rm = TRUE),
         ano = 2022) %>% 
  mutate(across(everything(), ~replace_na(., 0))) %>% 
  mutate(codmun = as.numeric(substr(codmun, 1, 6)),
         pop = popTotal) %>% 
  dplyr::select(-c(pop80a84, pop85a89, pop90a94, pop95a99, pop100oumais, popTotal)) %>% 
  mutate_at(vars(starts_with('pop'),-pop), funs("pct" = 100*(./pop))) %>%   # cria variaveis de percentual
  rename_at( vars( contains( "_pct") ), list( ~paste("pct", gsub("_pct", "", .), sep = "_") ) )%>%  # renomeia variaveis de percentual
  mutate(pct_pop65m   = pct_pop65a69+pct_pop70a74+pct_pop75a79+pct_pop80m,
         pct_pop75m   = pct_pop75a79+pct_pop80m,
         pct_pop5a14  = pct_pop5a9+pct_pop10a14,
         pct_pop15a64 = pct_pop15a19+pct_pop20a24+pct_pop25a29+pct_pop30a34+pct_pop35a39+pct_pop40a44+pct_pop45a49+pct_pop50a54+pct_pop55a59+pct_pop60a64)


# Junta bases
pop_final_anos <- pop_2010_2021 %>% bind_rows(pop_censo) %>% 
  filter(codmun > 0) %>% 
  dplyr::select(codmun, ano, pop)




# 1. Leitos: Tratar Dados - Abre e Prepara Dados ----------------------------------------------------------------------------------------------------

files = NULL

for (mes in meses) {
  
  files <- c(files, list.files(path = path_lt, pattern = paste0("*", mes, ".dbc"), full.names = T))
  
  
  
}

# Junta dados por mes/estado/ano e seleciona colunas
data <- purrr:::map(files, read.dbc, .progress = TRUE) %>%  # Le arquivos
  purrr:::map(select,CODUFMUN, QT_SUS, QT_NSUS, TP_LEITO,CODLEITO, COMPETEN) %>% # Seleciona colunas
  reduce(bind_rows) %>% 
  rename_all(tolower) %>%  # Tira capslock dos nomes das variaveis
  mutate(codufmun = as.numeric(as.character(codufmun))) %>% # Renomeia e transforma codigo de municipio em numerica
  rename(codmun = codufmun) %>% 
  mutate(codmun = ifelse(substr(codmun, start = 1, stop = 2)==53, 530010, codmun), # Imputa o código de Brasilia a municípios que são do DF mas com outro codigo de municipio
         ano = as.numeric(substr(competen, start = 1, stop = 4)),  # Gera variavel de ano
         codleito = as.numeric(codleito), # Transforma variaveis em numericas
         tp_leito = as.numeric(tp_leito)) %>%
  filter(codleito != 47) %>% # Descarta leitos psiquiatricos
  dplyr::select(-competen) %>% 
  mutate(n_leitouti_sus = ifelse(between(codleito, 61, 63) | between(codleito, 74, 83) | between(codleito, 85, 86),qt_sus, 0), # Gera variavel de Leitos de UTI nao-SUS
         n_leito_sus = qt_sus) # Gera variavel de Leitos SUS

# Colapsa por municipio/ano
data <- data %>% 
  group_by(codmun, ano) %>% 
  summarise(across(starts_with(c("n_leito")), sum))

# Salva base intermediaria
write_dta(data, path = sf_lt_temp)

rm(data)




# 2. Recursos Humanos: Tratar Dados - Abre e Prepara Dados ----------------------------------------------------------------------------------------------------

# Lista de anos e meses [dezembro] para serem tratados
meses <- c(paste0(2010:2022, "12"))


for (mes in meses) {
  
  print(paste0("starting for year ", mes, " at ", Sys.time()))
  
  
  tic()
  
  # conecta com a base
  profissionais_cnes <- arrow::open_dataset(path_dados_prof_cnes_parquet)
  
  # query 
  profissionais_cnes_parquet <- profissionais_cnes %>% 
  # seleciona colunas
  dplyr::select(CODUFMUN, CBO, COMPETEN, CPF_PROF, CPFUNICO, HORAOUTR, HORAHOSP, HORA_AMB, PROF_SUS) %>% 
  filter(COMPETEN %in% mes ) 
  
  
  # Baixa para ambiente do R
  data <- collect(profissionais_cnes_parquet) %>% 
    rename_all(tolower)
  
  #  Organiza variavel que identifica unicamente trabalhadores da saude no CNES
  #  Note que a ocupacao selecionada para ser unicamente identificada sob CPF_UNICO nao parece ter relacao com horas trabalhadas, ou nenhuma outra variavel na base de dados.
  #  A ocupacao parece estar sendo aleatoriamente selecionada entre todos os vinculos empregaticios que o individuo possui.
  #  Isso pode ser um problema ja que o emprego/ocupacao selecionado pode nao ser o principal.
  # Iremos usar essa variavel para contar trabalhadores da saude (medicos e enfermeiros), e disponibilizar um outro indicador padronizado por carga horaria.
  data <- data %>% 
    mutate(codufmun = as.numeric(codufmun)) %>% 
    rename(codmun = codufmun) %>% 
    mutate(codmun = ifelse(substr(codmun, start = 1, stop = 2)==53, 530010, codmun), # adjusment for brasilia
           ano= as.numeric(substr(competen, start = 1, stop = 4)), 
           codmun = as.numeric(codmun),
           ibge = codmun) %>% 
    right_join(haven::read_dta(path_reg_saude) %>% dplyr::select(regiao, ibge), by = "ibge") %>% # Faz o merge com regioes e macrorregioes
    right_join(haven::read_dta(path_macro_saude)%>% dplyr::select(macrorregiao, ibge), by = "ibge") 
  
  
  # Mantem apenas cargos relacionados a ocupacoes de medicos e enfermeiros, retirando ocupacoes administrativas
  data <- data %>% 
    ungroup()  %>% 
    filter(
      between(as.numeric(cbo), 6105, 6190) |
        between(as.numeric(cbo), 223101, 223157) |
        between(as.numeric(cbo), 225103, 225350) |
        cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") |
        cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1") |
        between(as.numeric(cbo), 7110, 7165) |
        between(as.numeric(cbo), 223505, 223565) |
        cbo %in% c("2235C1","2235C2","2235C3")
    )
  
  
  # A var de cpf unico esta a nivel da UF, mas queremos que fique a nivel do mun - e repetimos o procedimento para demais níveis geográficos
  
  # Novo identificador de 0/1 de cpf unico 
  
  # Mun
  data <- data %>% 
    arrange(codmun, ano) %>% 
    
    mutate(cpfunico = as.numeric(cpfunico), 
           cpfunico = if_else(is.na(cpfunico) == T,0,cpfunico)) %>% 
    
    group_by(cpf_prof, codmun) %>%
    mutate(cpfunico_mun = if_else(row_number() == 1, 1, 0)) %>%
    ungroup() %>%
    
    # Reg
    group_by(cpf_prof, regiao) %>%
    mutate(cpfunico_reg = if_else(row_number() == 1, 1, 0)) %>%
    ungroup() %>%
    
    # Macro
    group_by(cpf_prof, macrorregiao) %>%
    mutate(cpfunico_macro = if_else(row_number() == 1, 1, 0)) %>%
    ungroup() %>%
    
    # Br
    group_by(cpf_prof) %>%
    mutate(cpfunico_br = if_else(row_number() == 1, 1, 0)) %>%
    ungroup() 
  
  
  
  
  data <- data %>% 
    
    # Conta medicos unicos
    mutate(n_med = ifelse(cpfunico_mun == 1 & (between(as.numeric(cbo), 6105, 6190) |
                                                 between(as.numeric(cbo), 223101, 223157) |
                                                 between(as.numeric(cbo), 225103, 225350) |
                                                 cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") |
                                                 cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1")), 
                          1, 0),
           # Conta enfermeiros unicos
           n_enf = ifelse(cpfunico_mun == 1 & (between(as.numeric(cbo), 7110, 7165) |
                                                 between(as.numeric(cbo), 223505, 223565) |
                                                 cbo %in% c("2235C1","2235C2","2235C3")), 
                          1, 0),
           
           n_med = ifelse(is.na(n_med)==T, 0, n_med),
           n_enf = ifelse(is.na(n_enf)==T, 0, n_enf)
    )
  
  #toc()
  # ate aqui
  # Numero de medicos/enfermeiros padronizado
  # Organiza a variavel de horas de trabalho
  table(data[data$hora_amb >= 100, ]$hora_amb)
  table(data[data$horahosp >= 100, ]$horahosp)
  table(data[data$horaoutr >= 100, ]$horaoutr)
  
  
  #tic()
  # Horas de trabalho estao concentradas em 120, 180, 220, 360 and 440
  # Parecem ter um zero a mais
  # Portanto, vamos utilizar somente os dois primeiros digitos de seus valores
  data <- data %>%
    mutate(horaoutr = if_else(horaoutr >= 100, as.numeric(substr(horaoutr, 1, 2)), horaoutr),
           horahosp = if_else(horahosp >= 100, as.numeric(substr(horahosp, 1, 2)), horahosp),
           hora_amb = if_else(hora_amb >= 100, as.numeric(substr(hora_amb, 1, 2)), hora_amb)) 
  
  data <- data %>% 
    mutate(totalhr = rowSums(select(., horaoutr, horahosp, hora_amb))) # Gera variavel com o total de horas trabalhadas sob aquela ocupacao
  
  # Medicos e enfermeiros, considerando 40 horas de trabalho como padrao
  data <- data %>%
    mutate(n_med_ch = ifelse(between(as.numeric(cbo), 6105, 6190) | 
                               between(as.numeric(cbo), 223101, 223157) | 
                               between(as.numeric(cbo), 225103, 225350) | 
                               cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") | 
                               cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1"), 
                             totalhr/40, NA), 
           n_enf_ch = ifelse(between(as.numeric(cbo), 7110, 7165) | 
                               between(as.numeric(cbo), 223505, 223565) | 
                               cbo %in% c("2235C1","2235C2","2235C3"), 
                             totalhr/40, NA))
  
  
  # Transforma em data table para tornar mais rapido
  #data <- as.data.table(data)
  
  # Cria variavel auxiliar que da o numero de medicos/enfermeiros unicos por regiao
  data =  data %>% 
    mutate(n_med_reg = ifelse(cpfunico_reg == 1 & (between(as.numeric(cbo), 6105, 6190) | 
                                                     between(as.numeric(cbo), 223101, 223157) | 
                                                     between(as.numeric(cbo), 225103, 225350) | 
                                                     cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") | 
                                                     cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1")), 
                              1, 0),
           n_enf_reg = ifelse(cpfunico_reg == 1 & (between(as.numeric(cbo), 7110, 7165) | 
                                                     between(as.numeric(cbo), 223505, 223565) | 
                                                     cbo %in% c("2235C1","2235C2","2235C3")), 
                              1, 0),
           
           n_med_reg = ifelse(is.na(n_med_reg)==T, 0, n_med_reg),
           n_enf_reg = ifelse(is.na(n_enf_reg)==T, 0, n_enf_reg))
  
  
  # Cria variavel auxiliar que da o numero de medicos/enfermeiros unicos por macrorregiao
  data =  data %>% 
    mutate(n_med_macro = ifelse(cpfunico_macro == 1 & (between(as.numeric(cbo), 6105, 6190) | 
                                                         between(as.numeric(cbo), 223101, 223157) | 
                                                         between(as.numeric(cbo), 225103, 225350) | 
                                                         cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") | 
                                                         cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1")), 
                                1, 0),
           n_enf_macro = ifelse(cpfunico_macro == 1 & (between(as.numeric(cbo), 7110, 7165) | 
                                                         between(as.numeric(cbo), 223505, 223565) | 
                                                         cbo %in% c("2235C1","2235C2","2235C3")), 
                                1, 0),
           
           n_med_macro = ifelse(is.na(n_med_macro)==T, 0, n_med_macro),
           n_enf_macro = ifelse(is.na(n_enf_macro)==T, 0, n_enf_macro))
  
  
  # Cria variavel auxiliar que da o numero de medicos/enfermeiros unicos por uf
  data =  data %>% 
    mutate(n_med_uf = ifelse(cpfunico == 1 & (between(as.numeric(cbo), 6105, 6190) | 
                                                between(as.numeric(cbo), 223101, 223157) | 
                                                between(as.numeric(cbo), 225103, 225350) | 
                                                cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") | 
                                                cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1")), 
                             1, 0),
           n_enf_uf = ifelse(cpfunico == 1 & (between(as.numeric(cbo), 7110, 7165) | 
                                                between(as.numeric(cbo), 223505, 223565) | 
                                                cbo %in% c("2235C1","2235C2","2235C3")), 
                             1, 0),
           
           n_med_uf = ifelse(is.na(n_med_uf)==T, 0, n_med_uf),
           n_enf_uf = ifelse(is.na(n_enf_uf)==T, 0, n_enf_uf))
  
  # Cria variavel auxiliar que da o numero de medicos/enfermeiros unicos por br
  data =  data  %>% 
    mutate(n_med_br = ifelse(cpfunico_br == 1 & (between(as.numeric(cbo), 6105, 6190) | 
                                                   between(as.numeric(cbo), 223101, 223157) | 
                                                   between(as.numeric(cbo), 225103, 225350) | 
                                                   cbo %in% c("2231A1","2231A2","2231F3","2231F4","2231F5") | 
                                                   cbo %in% c("2231F6","2231F7","2231F8","2231F9","2231G1")), 
                             1, 0),
           n_enf_br = ifelse(cpfunico_br == 1 & (between(as.numeric(cbo), 7110, 7165) | 
                                                   between(as.numeric(cbo), 223505, 223565) | 
                                                   cbo %in% c("2235C1","2235C2","2235C3")), 
                             1, 0),
           
           n_med_br = ifelse(is.na(n_med_br)==T, 0, n_med_br),
           n_enf_br = ifelse(is.na(n_enf_br)==T, 0, n_enf_br))
  
  #toc()
  print(paste0("starting summarise for year ", mes, " at ", Sys.time()))
  
  # Colapsar dados por municipio/ano
  data <- data %>%
    group_by(codmun, ano) %>%
    summarize(
      across(c(n_med, n_enf, n_med_ch, n_enf_ch, n_med_reg, n_enf_reg, n_med_macro, n_enf_macro, n_med_uf, n_enf_uf, n_med_br, n_enf_br), ~sum(., na.rm = T)))#,
  #  across(c(n_med_reg, n_enf_reg, n_med_macro, n_enf_macro, n_med_uf, n_enf_uf, n_med_br, n_enf_br), ~n_distinct(., na.rm = TRUE)))
  
  # Salva temporário
  write_dta(data, path = paste0(sf_hr_temp, "hr_temp_", substr(mes,1,4), ".dta"))
  
  assign(value = data,  paste0("data_", substr(mes,1,4)))
  
  toc()
  
}

# Junta arquivos
data = mget(ls(pattern="data_[0-9].*"))

data = bind_rows(data)

# 2.2 Junta Bases ##############################################################

# Juntar bases de leitos e de RH
data_lt_rh <- read_dta(sf_lt_temp) %>% 
  right_join(pop_final_anos)


# Junta com base populacional e de regioes e macrorregioes
data_lt_rh <- data_lt_rh %>%
  #right_join(df_pop, by = c("codmun", "ano", "pop")) %>%
  mutate(n_leito_sus = replace(n_leito_sus, is.na(n_leito_sus), 0), # Transforma missings em zero
         n_leitouti_sus = replace(n_leitouti_sus, is.na(n_leitouti_sus), 0)) %>% 
  rename(ibge = codmun) %>%
  left_join(haven::read_dta(path_reg_saude) %>% dplyr::select(regiao, ibge), by = "ibge") %>%
  left_join(haven::read_dta(path_macro_saude) %>% dplyr::select(macrorregiao, ibge), by = "ibge") %>%
  rename(codmun = ibge) %>%
  mutate(id_estado = as.integer(substr(codmun, 1, 2)))


# 2.3 Agrega para niveis geograficos ###########################################

data_lt_rh_mun <- data_lt_rh %>% 
  left_join(data) %>% 
  mutate(
    # Calcular indicadores de taxas
    tx_med = 1000*(n_med/pop),
    tx_enf = 1000*(n_enf/pop),
    tx_med_ch = 1000*(n_med_ch/pop),
    tx_enf_ch = 1000*(n_enf_ch/pop),
    tx_leitouti_sus = 100000*(n_leitouti_sus/pop),
    tx_leito_sus = 100000*(n_leito_sus/pop)) %>% 
  dplyr::select(codmun, ano, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus, starts_with("tx_")) 


# Cria linhas com zeros para municipios pequenos que nao tem observacoes
data_lt_rh_mun <- data_lt_rh_mun %>%
  mutate(last = 0) %>%
  mutate(last = ifelse(ano == 2016 & (codmun == 521205 | codmun == 420325 | codmun == 420555) |
                         ano == 2018 & codmun == 240160, 1, last))  %>%
  mutate(across(-c(codmun,ano, last), ~if_else(last == 1, 0, .))) %>%
  ungroup() %>%
  select(-last)

# Coloca como Missing observacoes de Mojui dos Campos. Criado em 2013, ainda nao possuia dados neste ano
data_lt_rh_mun <- data_lt_rh_mun %>%
  mutate(mojui = 0,
         mojui = if_else(ano == 2013 & codmun == 150475, 1, mojui)) %>%
  mutate(across(-c(codmun, ano, mojui), ~if_else(mojui == 1, NA_real_, .))) %>%
  ungroup() %>%
  select(-mojui)


# Salvar
write_dta(data_lt_rh_mun, path = sf_codmun)



# 2.3 Agrega para regiao de saude   ###########################################

data_lt_rh_reg <- data_lt_rh %>% 
  left_join(data) %>% 
  dplyr::select(-n_med, -n_enf) %>% 
  mutate(n_med = n_med_reg,
         n_enf = n_enf_reg) %>% 
  group_by(regiao, ano) %>%
  summarize(across(c(pop, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus), ~sum(., na.rm = T)),
            n_med_reg = n_distinct(n_med_reg),
            n_enf_reg = n_distinct(n_enf_reg)) %>%
  mutate(across(c(n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus),
                ~ 1000 * (. / pop), .names = "tx_{col}"),
         across(c(n_leito_sus, n_leitouti_sus),
                ~ 100000 * (. / pop), .names = "tx_{col}")) %>%
  dplyr::select(ano, regiao, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus, tx_med = tx_n_med, tx_enf = tx_n_enf,
                tx_med_ch = tx_n_med_ch, tx_enf_ch = tx_n_enf_ch, tx_leito_sus = tx_n_leito_sus, tx_leitouti_sus = tx_n_leitouti_sus)

# Salvar
write_dta(data_lt_rh_reg, path = sf_codreg)


# 2.3 Agrega para macrorregiao de saude   ###########################################

data_lt_rh_macro <- data_lt_rh %>% 
  left_join(data) %>%
  dplyr::select(-n_med, -n_enf) %>% 
  mutate(n_med = n_med_macro,
         n_enf = n_enf_macro) %>% 
  group_by(macrorregiao, ano) %>%
  summarize(across(c(pop, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus), ~sum(., na.rm = T)),
            n_med_macro = n_distinct(n_med_macro),
            n_enf_macro = n_distinct(n_enf_macro)) %>%
  mutate(across(c(n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus),
                ~ 1000 * (. / pop), .names = "tx_{col}"),
         across(c(n_leito_sus, n_leitouti_sus),
                ~ 100000 * (. / pop), .names = "tx_{col}")) %>%
  dplyr::select(ano, macrorregiao, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus, tx_med = tx_n_med, tx_enf = tx_n_enf,
                tx_med_ch = tx_n_med_ch, tx_enf_ch = tx_n_enf_ch, tx_leito_sus = tx_n_leito_sus, tx_leitouti_sus = tx_n_leitouti_sus)
# Salvar
write_dta(data_lt_rh_macro, path = sf_codmacro)



# 2.3 Agrega para uf   ###########################################

data_lt_rh_uf <- data_lt_rh %>% 
  left_join(data) %>%
  dplyr::select(-n_med, -n_enf) %>% 
  mutate(n_med = n_med_uf,
         n_enf = n_enf_uf) %>% 
  group_by(id_estado, ano) %>%
  summarize(across(c(pop, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus), ~sum(., na.rm = T)),
            n_med_uf = n_distinct(n_med_uf),
            n_enf_uf = n_distinct(n_enf_uf)) %>%
  mutate(across(c(n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus),
                ~ 1000 * (. / pop), .names = "tx_{col}"),
         across(c(n_leito_sus, n_leitouti_sus),
                ~ 100000 * (. / pop), .names = "tx_{col}")) %>%
  dplyr::select(ano, id_estado, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus, tx_med = tx_n_med, tx_enf = tx_n_enf,
                tx_med_ch = tx_n_med_ch, tx_enf_ch = tx_n_enf_ch, tx_leito_sus = tx_n_leito_sus, tx_leitouti_sus = tx_n_leitouti_sus)


# Salvar
write_dta(data_lt_rh_uf, path = sf_coduf)


# 2.3 Agrega para br   ###########################################

data_lt_rh_br <- data_lt_rh %>% 
  left_join(data) %>%
  dplyr::select(-n_med, -n_enf) %>%
  mutate(n_med = n_med_br,
         n_enf = n_enf_br) %>% 
  group_by(ano) %>%
  summarize(across(c(pop, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus), ~ sum(., na.rm = TRUE)),
            n_med_br = n_distinct(n_med_br),
            n_enf_br = n_distinct(n_enf_br)) %>%
  mutate(across(c(n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus),
                ~ 1000 * (. / pop), .names = "tx_{col}"),
         across(c(n_leito_sus, n_leitouti_sus),
                ~ 100000 * (. / pop), .names = "tx_{col}")) %>%
  dplyr::select(ano, n_med, n_enf, n_med_ch, n_enf_ch, n_leito_sus, n_leitouti_sus, tx_med = tx_n_med, tx_enf = tx_n_enf,
                tx_med_ch = tx_n_med_ch, tx_enf_ch = tx_n_enf_ch, tx_leito_sus = tx_n_leito_sus, tx_leitouti_sus = tx_n_leitouti_sus)


# Salvar
write_dta(data_lt_rh_br, path = sf_brasil)