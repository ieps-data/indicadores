# IEPS DATA
#
# Esse codigo trata dados de mortalidade infantil do IBGE (UF e BR)



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------

rm(list=ls())
gc()

# Bibliotecas
xfun::pkg_attach(c('tidyverse','writexl', 'haven'), install=T)

# Setar diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/')

# Input
path_mortinf_ibge <- c("Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/B_morbidade_mortalidade/B1_mortinf/projecoes_2018_indicadores.xls")

# Output
path_output_mortinf_ibge_uf <- c("02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_uf.dta")
path_output_mortinf_ibge_br <- c("02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_br.dta")



# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

# UF

# Definir anos 
anos <- c("2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022")

# Definir estados
uf <- c("AC", "AL", "AP", "AM", "BA", "CE", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO", "DF")

# Cria data frame vazio para adicionar dados
mort_inf_ibge_uf <- data.frame(UF = character(), ano = numeric(), tx_mort_inf_ibge = numeric())

# Acrescenta dados no data frame por estado/ano
for (i in seq_along(uf)) {
  
  ibge_proj_uf <- read_excel(path_mortinf_ibge, sheet = uf[i], skip = 2)
  
  ibge_proj_uf <- ibge_proj_uf %>% 
    dplyr::select(Ano, `TAXA DE MORTALIDADE INFANTIL - TMI\n(‰)`) %>% 
    filter(Ano %in% anos) %>% 
    rename(ano = Ano,
           tx_mort_inf_ibge  = `TAXA DE MORTALIDADE INFANTIL - TMI\n(‰)`) %>% 
    mutate(ano = as.integer(ano),
           tx_mort_inf_ibge  = as.numeric(tx_mort_inf_ibge),
           UF   = uf[i]) 
  
  mort_inf_ibge_uf <- bind_rows(mort_inf_ibge_uf, ibge_proj_uf)
  
  
}


# Salvar 
mort_inf_ibge_uf %>%
  haven::write_dta(path_output_mortinf_ibge_uf)



# BR

# Junta dados nacionais por ano
mort_inf_ibge_br <- read_excel(path_mortinf_ibge, sheet = "Brasil", skip = 2)
  
mort_inf_ibge_br <- mort_inf_ibge_br %>% 
  dplyr::select(Ano, `TAXA DE MORTALIDADE INFANTIL - TMI\n(‰)`) %>% 
  filter(Ano %in% anos) %>% 
  rename(ano = Ano,
         tx_mort_inf_ibge  = `TAXA DE MORTALIDADE INFANTIL - TMI\n(‰)`) %>% 
  mutate(ano = as.integer(ano),
         tx_mort_inf_ibge  = as.numeric(tx_mort_inf_ibge)) 
  

# Salvar 
mort_inf_ibge_br %>%
  haven::write_dta(path_output_mortinf_ibge_br)


# FIM -----------------------------------------------------------------------------------------------------------------------------------------------