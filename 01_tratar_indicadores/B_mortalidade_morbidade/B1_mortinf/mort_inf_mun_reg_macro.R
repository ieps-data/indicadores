# IEPS DATA
#
# Esse codigo cria output vazio para Mortalidade Infantil mun regiao e macrorregiao de saude



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------

rm(list=ls())
gc()

# Bibliotecas
xfun::pkg_attach(c('data.table', 'ggplot2', 'dplyr', 'writexl', 'readxl','haven', 'readstata13'), install=T)


# Setar diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/')


# Input
path_muns  <-'02_temp_data/0_auxiliar/municipios.dta'
path_reg   <-'02_temp_data/0_auxiliar/Regioes_2022.dta'
path_macro <-'02_temp_data/0_auxiliar/Macrorregioes_2022.dta'

# Output
path_output_mortinf_mun     <- c("02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_mun.dta")
path_output_mortinf_reg     <- c("02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_reg.dta")
path_output_mortinf_macro   <- c("02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_macro.dta")



# 1. Ler Dados -----------------------------------------------------------------------------------------------------------------------------

# UNIDADES GEOGRAFICAS
reg   <- read.dta13(path_reg) %>%  # regiao de saude
  dplyr::select(ibge, regiao, no_regiao)
macro <-read.dta13(path_macro) %>% # macroregiao de saude
  dplyr::select(ibge, macrorregiao, no_macro)
muns <- read.dta13(path_muns) %>%  # municipio 
  dplyr::select(id_munic_7, id_munic_6, municipio, id_estado, estado_abrev, estado) %>% # tambem contem dados de estado
  dplyr::rename(codmun=id_munic_6,
                nomemun=municipio) 

# 2. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

reg <- reg %>% 
  distinct(regiao, .keep_all = TRUE) %>% 
  dplyr::select(-ibge)

macro <- macro %>% 
  distinct(macrorregiao, .keep_all = TRUE) %>% 
  dplyr::select(-ibge)



# Criar base vazia municipal
mortinf_mun <- data.frame(codmun = as.numeric(rep(unique(muns$codmun)), each = 10), 
                       ano = as.numeric(rep(c(2010:2022), each = length(unique(muns$codmun)))),
                       tx_mort_inf_ibge = NA)

mortinf_mun %>% write_dta(path_output_mortinf_mun)



# Criar base vazia regiao de saude
mortinf_reg <- data.frame(regiao = as.numeric(rep(unique(reg$regiao)), each = 10), 
                       ano = as.numeric(rep(c(2010:2022), each = length(unique(reg$regiao)))),
                       tx_mort_inf_ibge = NA)

mortinf_reg %>% write_dta(path_output_mortinf_reg)



# Criar base vazia macrorregiao de saude
mortinf_macro <- data.frame(macrorregiao = as.numeric(rep(unique(macro$macrorregiao)), each = 10), 
                         ano = as.numeric(rep(c(2010:2022), each = length(unique(macro$macrorregiao)))),
                         tx_mort_inf_ibge = NA)

mortinf_macro %>% write_dta(path_output_mortinf_macro)