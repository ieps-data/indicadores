library(dplyr)


# Cria Programa para classificar causas evitaveis de obitos

# CID-10: http://apps.who.int/classifications/icd10/browse/2016/
 # Classificaçao baseada em Nolte & McKee (2003, 2008, 2011)
# Nolte E, McKee CM. Measuring the health of nations: updating an earlier analysis. Health Aff (Millwood) 2008;27:58-71.
# Nolte E, McKee M. Measuring the health of nations: analysis of mortality amenable to health care. Bmj 2003;327:1129.
# Nolte E, McKee M. Variations in amenable mortality--trends in 16 high-income nations. Health Policy 2011;103:47-52.

# Cria programa
evitaveis <- function(df, var_cid, nova_var, idade_var) {
  
  data2 = df 
  
  data2 <- data2 %>% 
  mutate(!!sym(new_var) :=  case_when( # Intestinal infections
    !!rlang::sym(idade_var) <= 414 & str_detect(!!rlang::sym(var_cid), "A0") ~ 1, 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "A1[56789]|B90") ~ 1,  # Tuberculosis
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "A35|A36|A80") ~ 1, # * Other infections (diphteria, tetanus, poliomyelitis)
    !!rlang::sym(idade_var) <= 414 & str_detect(!!rlang::sym(var_cid), "A37") ~ 1, # Whooping cough 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "A40|A41") ~ 1, # Septicaemia 
    !!rlang::sym(idade_var) > 400 &  !!rlang::sym(idade_var) < 415 &  str_detect(!!rlang::sym(var_cid), "B05") ~ 1, # Measles 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C1[89]|C20|C21") ~ 1, # Malignant neoplasm of colon and rectum
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C44") ~ 1, #  Malignant neoplasm of skin
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C50") ~ 1, #  Malignant neoplasm of breast
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C53") ~ 1, # Malignant neoplasm of uteri
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C54|C55") ~ 1, # Malignant neoplasm of cervix uteri and body of uterus
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C62") ~ 1, # Malignant neoplasm of testis
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "C81") ~ 1, # Hodgkin's disease
    !!rlang::sym(idade_var) <= 444 & str_detect(!!rlang::sym(var_cid), "C9[12345]") ~ 1, # Leukaemia 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "E0[01234567]") ~ 1, # Diseases of the thyroid
    !!rlang::sym(idade_var) <= 449 & str_detect(!!rlang::sym(var_cid), "E1[01234]") ~ 1, # Diabetes mellitus
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "G40|G41") ~ 1, # Epilepsy 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "I0[56789]") ~ 1, # Chronic rheumatic heart disease
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "I1[01235]") ~ 1, # Hypertensive disease
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "I60") ~ 1, # Cerebrovascular disease
    !!rlang::sym(idade_var) > 400 &  !!rlang::sym(idade_var) < 415 & str_detect(!!rlang::sym(var_cid), "J0|J20|J30|J40|J50|J60|J70|J80|J90") ~ 1, # All respiratory diseases (excluding pneumonia and influenza)	
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "J10|J11") ~ 1, # Influenza
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "J12|J13|J14|J15|J16|J17|J18") ~ 1, # Pneumonia 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "K2[567]") ~ 1, # Peptic ulcer 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "K3[56789]") ~ 1, # Appendicitis
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "K4[0123456]") ~ 1, # Abdominal hernia 
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "K8[01]") ~ 1, # Cholelithiasis and cholecystitis
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "N0[01234567]|N1[789]|N2[567]") ~ 1, # Nephritis and nephrosis
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "N40") ~ 1, # Benign prostatic hyperplasia
    str_detect(!!rlang::sym(var_cid), "^0") ~ 1, # Maternal death
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "Q2[012345678]") ~ 1, # Congenital cardiovascular anomalies
    str_detect(!!rlang::sym(var_cid), "A33|P00|P[12345678]|P9[0123456]") ~ 1, # Perinatal deaths, all causes, excluding stillbirths
    str_detect(!!rlang::sym(var_cid), "Y6|Y83|Y84") ~ 1, # Misadventures to patients during surgical and medical care
    !!rlang::sym(idade_var) <= 474 & str_detect(!!rlang::sym(var_cid), "I20|I21|I22|I23|I24|I25") ~ 1, # Ischaemic heart disease
    TRUE ~ 0
  ))

  return(data2)
  
  }

