
# IEPS DATA

# Esse codigo trata os dados de hospitalizações

# 0. Config -----------------------------------------------------------------------------------------------------------------------------------------

rm(list=ls())
gc()

# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'haven', 'purrr', 'furrr', 'rlang', 'tictoc', 'foreign'), install=T)


# Diretorio
setwd("Z:/")


#Input
path_sih <- "I:/dados_ieps/dados_brutos/ministerio_da_saude/datasus/sih/dados/aih_reduzida/"
path_pop_idade <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2021.dta"
path_pop_censo <- "Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/G_demografia/pop_idade/pop_censo_2022.xlsx"
path_reg_saude <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Regioes_2022.dta"
path_macro_saude <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Macrorregioes_2022.dta"

#Output
sih_temp <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/"
sih_mun <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_mun.dta"
sih_reg <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_reg.dta"
sih_macro <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_macro.dta"
sih_uf <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_uf.dta"
sih_br <- "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_br.dta"

# Programas auxiliares
#source("Projects/IEPS Data/Indicadores/indicadores-codigos/01_tratar_indicadores/B_mortalidade_morbidade/auxiliar/programa_csap.R")


############################################# CSAP

# Program define csap

library(rlang)

# Program define csap
csap <- function(df, var_cid, new_var) {
  
  data2 = df
  
  #var_cid = !!(sym(var_cid))
  
  data2 <- data2 %>%
    mutate(!!sym(new_var) :=  case_when(str_detect(!!rlang::sym(var_cid), "A0") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A170|A18|A19") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A150|A151|A152|A153|A160|A161|A162") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A154|A155|A156|A157|A158|A159") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A163|A164|A165|A166|A167|A168|A169") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A171|A172|A173|A174|A175|A176|A177|A178|A179") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A36|A37|A33|A34|A35") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A46|A50|A51|A52|A53") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "A95") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "B26|B06|B05|B16") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "B50|B51|B52|B53|B54") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "B77") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "D50") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "E10|E11|E12|E13|E14") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "E40|E41|E42|E43|E44|E45|E46") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "E5|E60|E61|E62|E63|E64") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "E86") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "G000") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "G40|G41") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "G45|G46") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "H66") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "I00|I01|I02") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "I10|I11|I20|I50") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "I63|I64|I65|I66|I67|I69") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "J00|J01|J02|J03|J06") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "J13|J14|J153|J154|J158|J159|J181") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "J20|J21") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "J31|J40|J41|J42|J43|J44|J45|J46|J47") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "J81") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "K25|K26|K27|K28|K920|K921|K922") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "L01|L02|L03|L04|L08") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "N10|N11|N12|N30|N34|N390|N70|N71|N72|N73|N75|N76") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "O23") ~ 1,
                                        str_detect(!!rlang::sym(var_cid), "P350") ~ 1,
                                        TRUE ~ 0
    ))
  
  
  
  
  
  return(data2)
  
}


# # Dados de população ------------------------------------------------------------------------------------------------------------------------------

pop_2010_2021 <- haven::read_dta(path_pop_idade) 

# Dados pop 2022 Censo
pop_censo  <-   read_xlsx(path_pop_censo, skip = 4)    %>% 
  rename(codmun = ...1) %>% 
  dplyr::select(-...2, -...3, -...4 ) %>% 
  rename_with(~str_replace(.x, "anos", ""), -codmun) %>% 
  rename_with(~paste0("pop", .x), -codmun) %>% 
  rename_with(~gsub("\\s+", "", .x), everything()) %>% 
  mutate(across(everything(), as.numeric)) %>% 
  rowwise() %>%
  mutate(pop80m = sum(c(pop80a84, pop85a89, pop90a94, pop95a99, pop100oumais), na.rm = TRUE),
         ano = 2022) %>% 
  mutate(across(everything(), ~replace_na(., 0))) %>% 
  mutate(codmun = as.numeric(substr(codmun, 1, 6)),
         pop = popTotal) %>% 
  dplyr::select(-c(pop80a84, pop85a89, pop90a94, pop95a99, pop100oumais, popTotal)) %>% 
  mutate_at(vars(starts_with('pop'),-pop), funs("pct" = 100*(./pop))) %>%   # cria variaveis de percentual
  rename_at( vars( contains( "_pct") ), list( ~paste("pct", gsub("_pct", "", .), sep = "_") ) )%>%  # renomeia variaveis de percentual
  mutate(pct_pop65m   = pct_pop65a69+pct_pop70a74+pct_pop75a79+pct_pop80m,
         pct_pop75m   = pct_pop75a79+pct_pop80m,
         pct_pop5a14  = pct_pop5a9+pct_pop10a14,
         pct_pop15a64 = pct_pop15a19+pct_pop20a24+pct_pop25a29+pct_pop30a34+pct_pop35a39+pct_pop40a44+pct_pop45a49+pct_pop50a54+pct_pop55a59+pct_pop60a64)


# Junta bases
pop_final_anos <- pop_2010_2021 %>% bind_rows(pop_censo) %>% 
  filter(codmun > 0)



# 1.1 Tratar Dados - Abre e Prepara Dados -----------------------------------------------------------------------------------------------------------

# Define lista de anos, meses e estados a serem tratados
anos <- c("10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23")
#meses <- c("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
estados <- c("AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO")


# Loop para ler e tratar arquivos separado por ano por razões computacionais

for (a in seq_along(anos)) {

  files = NULL


for (uf in seq_along(estados)) {


    prefix <- "rd"

    pattern <- c(paste0(prefix, tolower(estados[uf]), anos[a], "\\d\\d", "*.dbc"))

    print(paste0("starting to list for ", estados[uf], " ", anos[a]))

    files <- c(files, list.files(path = path_sih, pattern = pattern, full.names = T))

}

  # paraleliza para aumentar eficiencia e reduzir tempo de codigo
  no_cores <- availableCores() - 1
  plan(multicore, workers = no_cores)

  # idenifica etapa
  print(paste0("starting for year ", anos[a]))


  # Junta dados por mes/estado/ano e seleciona colunas
  data <- furrr::future_map(files, read.dbc, .progress = TRUE) %>%  # Le arquivos
    purrr:::map(select,DT_INTER, MUNIC_RES, MUNIC_MOV, DIAG_PRINC, IDENT) %>% # Seleciona colunas
    purrr:::map(mutate, ano = as.numeric(substr(DT_INTER, 1, 4))) %>%  # Cria variavel de ano
    purrr:::map(mutate, ano = ifelse(is.na(ano) == T, 0, ano)) %>%
    purrr:::map(mutate, IDENT = as.numeric(IDENT)) %>%
    purrr:::map(mutate, IDENT = ifelse(is.na(IDENT) == T, 0 , IDENT)) %>%
    purrr:::map(filter, ano >= 2010 &  ano < 2023) %>% # Baixamos dados de 2023 para incorporar hospitalizacoes de 2021 que aparecem apenas nos arquivos de 2022
    purrr:::map(filter, IDENT != 5) %>%  # Exclui internacoes + de 1 mes (paciente que continua internado + de 1 mes, ident = 5)
    reduce(bind_rows)

  data <- data %>%
    rename_all(tolower) %>%  # Tira capslock dos nomes das variaveis
    mutate(munic_res = as.numeric(as.character(munic_res)), # Muda classe da variavel de municipio de residencia
           munic_res = ifelse(substr(munic_res, 1, 2) == 53, 530010, munic_res),
           n_hosp_total = 1) # Total de Hospitalizações

  # Identifica hospitalizacoes por Causas Sensiveis a Atencao Primaria (CSAP)
  data <- csap(df = data, var_cid = "diag_princ", new_var = "n_hosp_csap")

  #Hospitalizacoes por Causas Mal Definidas

  # classificacao segue  https://www.who.int/data/gho/indicator-metadata-registry/imr-details/1215
  # data <- data %>%
  #   mutate(n_hosp_maldef = case_when(str_detect(diag_princ, "^R") ~ 1,0))


  data <- data %>%
    rename(codmun = munic_res) %>%  # Renomeia variavel de codigo municipal e altera seu formato
  #  mutate(codmun = as.numeric(codmun)) %>%
    group_by(codmun, ano) %>%
    summarize(n_hosp_total = sum(n_hosp_total), # Agrupa no nivel do municipio e ano
              n_hosp_csap = sum(n_hosp_csap))

  # Salva arquivo temporario
  write.dta(data, file = paste0(sih_temp, "sih_tempAgg_", anos[a], ".dta"))

  gc()

}

rm(data, pattern, files)

# Reune arquivos de todos os anos
files <- list.files(path = sih_temp, pattern = "sih_tempAgg_*", full.names = T)

# Inicia paralelizacao
no_cores <- availableCores() - 1
plan(multicore, workers = no_cores)

# Abre arquivos
data <- furrr::future_map(files, read_dta, .progress = TRUE) %>% 
  reduce(bind_rows)



# 1.2 Junta bases e agrega para municipio/ano -------------------------------------------------------------------------------------------------------

# Agrupa no nivel do municipio e ano
data <- data %>% 
  group_by(codmun, ano) %>% 
  summarize(n_hosp_total = sum(n_hosp_total),
            n_hosp_csap = sum(n_hosp_csap))


# Abre arquivos de regiao e macrorregiao e saude
reg_saude <- read_dta(file = path_reg_saude)
macro_saude <- read_dta(file = path_macro_saude)

# Faz o merge com dados de unidades geograficas e de populacao por idade
data <- data %>% 
  rename(ibge = codmun) %>% 
  left_join(reg_saude %>%  dplyr::select(ibge, regiao), by = "ibge") %>% 
  left_join(macro_saude %>%  dplyr::select(ibge, macrorregiao), by = "ibge") %>% 
  rename(codmun = ibge) %>% 
  mutate(id_estado = as.numeric(substr(as.character(codmun), 1,2))) %>% 
  left_join(pop_final_anos, by = c("codmun", "ano")) %>% # Merge com dados de população por idade
  drop_na(regiao, macrorregiao)


# Cria taxas
data_mun <- data %>% 
  mutate(tx_hosp = 100000*(n_hosp_total/pop),
         tx_hosp_csap = 100000*(n_hosp_csap/pop)) %>% 
  rename(n_hosp = n_hosp_total) # Muda nome de hosp totais


# Proporcao de hosp mal definidas
# data_mun <- data_mun %>% 
#   mutate(pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp))

# Seleciona variaveis
data_mun <-  data_mun %>% 
  dplyr::select(codmun, ano, n_hosp, n_hosp_csap, tx_hosp, tx_hosp_csap)

# Salvar
write_dta(data_mun, path = sih_mun)

rm(data_mun)



# 1.3 Agrega para demais niveis geograficos -------------------------------------------------------------------------------------------------------

# Agrupa dados no nível de regiao-ano
data_reg <- data %>% 
  group_by(regiao, ano) %>% 
  summarize(n_hosp_total = sum(n_hosp_total),
            n_hosp_csap = sum(n_hosp_csap),
            pop   = sum(pop))


# Cria taxas
data_reg <- data_reg %>% 
  mutate(tx_hosp = 100000*(n_hosp_total/pop),
         tx_hosp_csap = 100000*(n_hosp_csap/pop)) %>% 
  rename(n_hosp = n_hosp_total) # Muda nome de hosp totais


# Proporcao de hosp mal definidas
# data_reg <- data_reg %>% 
#   mutate(pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp))

# Seleciona variaveis
data_reg <-  data_reg %>% 
  dplyr::select(regiao, ano, n_hosp, n_hosp_csap, tx_hosp, tx_hosp_csap)


# Salvar
write_dta(data_reg, path = sih_reg)



# Agrega para macrorregiao de saude

# Agrupa dados no nível de macro-ano
data_macro <- data %>% 
  group_by(macrorregiao, ano) %>% 
  summarize(n_hosp_total = sum(n_hosp_total),
            n_hosp_csap = sum(n_hosp_csap),
            pop  = sum(pop))



# Cria taxas
data_macro <- data_macro %>% 
  mutate(tx_hosp = 100000*(n_hosp_total/pop),
         tx_hosp_csap = 100000*(n_hosp_csap/pop)) %>% 
  rename(n_hosp = n_hosp_total) # Muda nome de hosp totais


# Proporcao de hosp mal definidas
# data_macro <- data_macro %>% 
#   mutate(pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp))

# Seleciona variaveis
data_macro <-  data_macro %>% 
  dplyr::select(macrorregiao, ano, n_hosp, n_hosp_csap, tx_hosp, tx_hosp_csap)


# Salvar
write_dta(data_macro, path = sih_macro)


# Agrega para UF

# Agrupa dados no nível de uf-ano
data_uf <- data %>% 
  group_by(id_estado, ano) %>% 
  summarize(n_hosp_total = sum(n_hosp_total),
            n_hosp_csap = sum(n_hosp_csap),
            pop   = sum(pop))



# Cria taxas
data_uf <- data_uf %>% 
  mutate(tx_hosp = 100000*(n_hosp_total/pop),
         tx_hosp_csap = 100000*(n_hosp_csap/pop)) %>% 
  rename(n_hosp = n_hosp_total) # Muda nome de hosp totais


# Proporcao de hosp mal definidas
# data_uf <- data_uf %>% 
#   mutate(pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp))

# Seleciona variaveis
data_uf <-  data_uf %>% 
  dplyr::select(id_estado, ano, n_hosp, n_hosp_csap, tx_hosp, tx_hosp_csap)


# Salvar
write_dta(data_uf, path = sih_uf)


# Agrega para o Brasil

# Agrupa dados no nível de br-ano
data_br <- data %>% 
  group_by(ano) %>% 
  summarize(n_hosp_total = sum(n_hosp_total),
            n_hosp_csap = sum(n_hosp_csap),
            pop   = sum(pop))

# Cria taxas
data_br <- data_br %>% 
  mutate(tx_hosp = 100000*(n_hosp_total/pop),
         tx_hosp_csap = 100000*(n_hosp_csap/pop)) %>% 
  rename(n_hosp = n_hosp_total) # Muda nome de hosp totais


# Proporcao de hosp mal definidas
# data_uf <- data_uf %>% 
#   mutate(pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp))

# Seleciona variaveis
data_br <-  data_br %>% 
  dplyr::select(ano, n_hosp, n_hosp_csap, tx_hosp, tx_hosp_csap)


# Salvar
write_dta(data_br, path = sih_br)
