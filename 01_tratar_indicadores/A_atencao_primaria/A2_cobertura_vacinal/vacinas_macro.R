
# IEPS DATA
#
# Esse codigo trata dados de macroregiao de saude de vacinas  



# 0. Configuração---------------------------------------

rm(list=ls())
gc()

# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here', 'haven','stringi'), install=T)


# Diretório
dir <- "Z:/Projects/IEPS Data/Indicadores/Data/"


# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

# Setar ano final a ser baixado
ano_final <- 2022

# Criar dataframe vazio para armazenar dados
vacinas_macro_ano <- NULL

# Loop para Tratar os dados
for (ano in 2010:ano_final) {
  
  # Abrir os dados
  vacinas_macro <- read_xlsx(stringr::str_c(dir,"01_input_data/A_atencao_basica/A2_cobertura_vacinal/macro_",ano,".xlsx"), skip = 3)
  
  # Adicionar coluna de ano
  vacinas_macro <- vacinas_macro%>%
    mutate(ano = ano)
  
  # Juntar anos
  vacinas_macro_ano <- bind_rows(vacinas_macro_ano, vacinas_macro)
  
}    


# Preparar pra salvar
vacinas_macro_ano <- vacinas_macro_ano %>%
  
  mutate(cob_vac_hepb = ifelse(ano < 2022, `Hepatite B  em crianças até 30 dias`, `Hepatite B idade <= 30 dias`)) %>% 
  
  # Renomear colunas
  dplyr::rename(macro = "Macrorregião de Saúde",
         cob_vac_bcg = 'BCG',
         cob_vac_rota = 'Rotavírus Humano',
         cob_vac_menin = 'Meningococo C',
         cob_vac_pneumo = 'Pneumocócica',
         cob_vac_polio = 'Poliomielite',
         cob_vac_tvd1 = 'Tríplice Viral  D1',
         cob_vac_penta = 'Penta',
     #    cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
         cob_vac_hepa = 'Hepatite A')%>%
  
  # Retirar linha de totais
  filter(macro != 'Total')%>%
  
  # Mudar separador decimal e criar coluna de codigo municipal
  mutate(macrorregiao = as.numeric(substr(macro, 1, 4)),
         id_estado = as.numeric(substring(macrorregiao, first = 1, last = 2)),
         no_macro = substring(macro, 6),
         no_macro = str_replace(string = no_macro, pattern = "Âª",replacement =  "ª"),
         no_macro = stri_trans_general(tolower(no_macro), "latin-ascii"))%>%
  select(macrorregiao, id_estado, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>% 
  
  
  # Considerar coberturas acima de 100% como 100%
  mutate_at(.vars=c('cob_vac_bcg','cob_vac_rota','cob_vac_menin','cob_vac_pneumo','cob_vac_polio','cob_vac_tvd1','cob_vac_penta','cob_vac_hepb','cob_vac_hepa'),
            .funs=function(x) {ifelse(x>=100,100,x)})





# Salvar base final
write_dta(vacinas_macro_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_macro.dta'))


# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
