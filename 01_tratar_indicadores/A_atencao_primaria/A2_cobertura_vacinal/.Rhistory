#
#
# map_uf_macro <-
#   macro_mapas %>%
#   filter(code_state == codes_states[i]) %>%
#   select(-code_state) %>%
#   dplyr::rename(macro = macrorregiao)
#
# st_write(map_uf_macro, paste0(path_out_shp, 'macro/', "map_", abbrev_state, "_macro.shp"))
}
# 0. Config --------------------------------------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('dplyr', 'tidyr','readr', 'haven','purrr','stringr', 'readstata13'), install=T)
# Diretorio
path <- 'R:/Projects/IEPS Data/Indicadores/Data/'
# Input
path_demografia <- stringr::str_c(path,'02_temp_data/G_demografia/pop_tabnet_idade_2010_2021.dta')
path_reg   <- stringr::str_c(path,'02_temp_data/0_auxiliar/Regioes_2022.dta')
path_macro <-stringr::str_c(path,'02_temp_data/0_auxiliar/Macrorregioes_2022.dta')
path_muns       <- stringr::str_c(path,'02_temp_data/0_auxiliar/municipios.dta')
# 1. Baixar dados --------------------------------------------------------------
pbf_download <- function(ano){
readr::read_csv(file = stringr::str_c('https://aplicacoes.mds.gov.br/sagi/servicos/misocial?fq=anomes_s:',ano,'*&fq=tipo_s:mes_mu&wt=csv&q=*&fl=ibge:codigo_ibge,anomes:anomes_s,qtd_familias_beneficiarias_bolsa_familia,valor_repassado_bolsa_familia&rows=10000000&sort=anomes_s%20asc,%20codigo_ibge%20asc'))
}
pbf <-  tibble(ano = 2010:2021) %>%
purrr::map_df(.x= .$ano, .f=~pbf_download(ano = .x))
# 2. Salvar --------------------------------------------------------------------
readr::write_csv(pbf,stringr::str_c(path,'01_input_data/F_socioeconomicos/F2_bolsa_familia/pbf.csv'))
# 3. Le arquivos --------------------------------------------------------------------------------------------------------------------------------------------
reg   <-read.dta13(path_reg)
macro <-read.dta13(path_macro)
muns  <-read.dta13(path_muns) %>%
dplyr::select(id_munic_6, estado_abrev, estado)
# Abrir dados populacionais
pop       <- haven::read_stata(path_demografia) %>%
dplyr::select(ano, codmun, pop)
# Preparar base de bolsa familia
pbf_temp_valor <- pbf %>%
# Criar novas variaveis
mutate(ano = as.numeric(str_sub(anomes, 1,4)),
mes = str_sub(anomes, 5, 6)) %>%
# Selecionar variaveis
select(codmun = ibge, ano,
valor = valor_repassado_bolsa_familia) %>%
group_by(codmun, ano) %>%
summarize_if(is.numeric, .funs='sum',na.rm=T) %>%  # soma por ano o numero de beneficiarios e valor pago
left_join(pop, by = c("codmun", "ano"))   # junta dados de populacao municipal
# Preparar base de bolsa familia
pbf_temp_benef_21 <- pbf %>%
# Criar novas variaveis
mutate(ano = as.numeric(str_sub(anomes, 1,4)),
mes = as.numeric(str_sub(anomes, 5, 6))) %>%
# Selecionar variaveis
select(codmun = ibge, ano, mes, num_familias_bf = qtd_familias_beneficiarias_bolsa_familia,
valor = valor_repassado_bolsa_familia) %>%
filter(mes == 10 & ano ==2021) %>%
dplyr::select(codmun, ano, num_familias_bf)
# Preparar base de bolsa familia
pbf_temp_benef_20 <- pbf %>%
# Criar novas variaveis
mutate(ano = as.numeric(str_sub(anomes, 1,4)),
mes = as.numeric(str_sub(anomes, 5, 6))) %>%
# Selecionar variaveis
select(codmun = ibge, ano, mes, num_familias_bf = qtd_familias_beneficiarias_bolsa_familia,
valor = valor_repassado_bolsa_familia) %>%
filter(mes == 12 & ano < 2021) %>%
dplyr::select(codmun, ano, num_familias_bf)
pbf_temp_benef <- pbf_temp_benef_20 %>%
bind_rows(pbf_temp_benef_21)
pbf_temp <- pbf_temp_valor %>%
full_join(pbf_temp_benef, by = c("codmun", "ano"))
# Salvar dados municipais
pbf_mun <- pbf_temp %>%
mutate(gasto_pbf_pc = valor/pop) %>%
dplyr::select(codmun, ano, num_familias_bf, gasto_pbf_pc)
# Salvar
readstata13::save.dta13(pbf_mun, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_mun.dta'))
# Parte 3. Agregar para niveis de regiao de saude -----------------------------------------------------------------------------------------------------------------------------
# 3.1) Região de Saúde
pbf_reg <- pbf_temp %>%
left_join(reg, by = c('codmun'='ibge')) %>% # junta informacao de regiao de saude
group_by(regiao, ano) %>%
summarize_if(is.numeric, .funs='sum',na.rm=T) %>%
mutate(gasto_pbf_pc = valor/pop) %>%
dplyr::select(regiao, ano, num_familias_bf, gasto_pbf_pc)
# Salvar
readstata13::save.dta13(pbf_reg, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_reg.dta'))
# 3.2) Macrorregião de Saúde
pbf_macro <- pbf_temp %>%
left_join(macro, by = c('codmun'='ibge')) %>% # junta informacao de macrorregiao de saude
group_by(macrorregiao, ano) %>%
summarize_if(is.numeric, .funs='sum',na.rm=T) %>%
mutate(gasto_pbf_pc = valor/pop) %>%
dplyr::select(macrorregiao, ano, num_familias_bf, gasto_pbf_pc)
# Salvar
readstata13::save.dta13(pbf_macro, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_macro.dta'))
# 3.3) Estado
pbf_uf <- pbf_temp %>%
left_join(muns, by = c('codmun'='id_munic_6')) %>% # junta informacao de regiao de saude
group_by(estado_abrev, ano) %>%
summarize_if(is.numeric, .funs='sum',na.rm=T) %>%
mutate(gasto_pbf_pc = valor/pop) %>%
dplyr::select(estado_abrev, ano, num_familias_bf, gasto_pbf_pc)
# Salvar
write_dta(pbf_uf, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_uf.dta'))
# 3.4) Nível nacional
pbf_br <- pbf_temp %>%
group_by(ano) %>%
summarize_if(is.numeric, .funs='sum',na.rm=T) %>%
mutate(gasto_pbf_pc = valor/pop) %>%
dplyr::select(ano,num_familias_bf, gasto_pbf_pc)
# Salvar
readstata13::save.dta13(pbf_br, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_br.dta'))
View(pbf_reg)
View(pbf_macro)
source("~/GitHub/indicadores/01_tratar_indicadores/F_socioeconomicos/F2_bolsa_familia/deflacionar_bolsa_familia.R", echo=TRUE)
source("~/GitHub/indicadores/01_tratar_indicadores/F_socioeconomicos/F1_ideb/censo_matriculas.R", echo=TRUE)
source("~/GitHub/indicadores/01_tratar_indicadores/F_socioeconomicos/F1_ideb/ideb_br.R", echo=TRUE)
source("~/GitHub/indicadores/01_tratar_indicadores/F_socioeconomicos/F1_ideb/ideb_mun.R", echo=TRUE)
source("~/GitHub/indicadores/01_tratar_indicadores/F_socioeconomicos/F1_ideb/ideb_reg_macro_ponderar.R", echo=TRUE)
source("~/GitHub/indicadores/01_tratar_indicadores/F_socioeconomicos/F1_ideb/ideb_uf.R", echo=TRUE)
# IEPS DATA
#
# Esse codigo trata dados de regiao nacionais vacinas
# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here'), install=T)
# Diretório
dir <- here::here('Data//')
dir
# IEPS DATA
#
# Esse codigo trata dados de regiao nacionais vacinas
# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here'), install=T)
# Diretório
dir <- 'Z:/Projects/IEPS Data/Indicadores/Data/'
# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------
# Setar ano final a ser baixado
ano_final <- 2021
# Criar dataframe vazio para armazenar dados
vacinas_br_ano <- NULL
# Loop para tratar os dados
for (ano in 2010:ano_final) {
# Abrir os dados
vacinas_br <- read_xlsx(stringr::str_c(dir, "/01_input_data/A_atencao_basica/A2_cobertura_vacinal/br_",ano,".xlsx"), skip = 3)
# Reshape os dados do formato 'long' para 'wide
vacinas_br <- vacinas_br %>%
filter(!is.na(Total)) %>%
dplyr::select(Imuno, Total) %>%
spread(Imuno, Total) %>%
mutate(ano = ano) # cria coluna de ano
# Juntar anos
vacinas_br_ano <- bind_rows(vacinas_br_ano, vacinas_br)
}
# Preparar pra salvar
vacinas_br_ano <- vacinas_br_ano %>%
# Renomear colunas
dplyr::rename(cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A')%>%
# Mudar separador decimal
dplyr::mutate(across(.cols = !ano, ~stringr::str_replace_all(string = .,
pattern = ',',
replacement = '.') %>%
as.numeric()))%>%
dplyr::select(ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>%
# Considerar coberturas acima de 100% como 100%
dplyr::mutate(across(.cols = !ano, ~case_when(.>100 ~ 100,
T ~.)))
# Salvar base final
readstata13::save.dta13(vacinas_br_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_br.dta'))
# Como função:
limpar_vacinas_br <- function(ano){
readxl::read_xlsx(stringr::str_c(dir, "/01_input_data/A_atencao_basica/A2_cobertura_vacinal/br_",ano,".xlsx"), skip = 3) %>%
filter(!is.na(Total)) %>%
dplyr::select(Imuno, Total) %>%
tidyr::pivot_wider(names_from = Imuno,
values_from = Total) %>%
dplyr::mutate(ano = ano) %>%
dplyr::mutate(across(.cols = !ano, ~stringr::str_replace_all(string = .,
pattern = ',',
replacement = '.') %>%
as.numeric())) %>%
dplyr::mutate(across(.cols = !ano, ~case_when(.>100 ~ 100,
T ~.)))
}
vacinas_br_ano <- list(ano = c(2010:2021)) %>%
purrr::pmap_dfr(.l = ., .f= limpar_vacinas_br) %>%
dplyr::rename(cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A') %>%
dplyr::select(ano, cob_vac_bcg, cob_vac_rota,
cob_vac_menin, cob_vac_pneumo,
cob_vac_polio, cob_vac_tvd1,
cob_vac_penta, cob_vac_hepb, cob_vac_hepa)
# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
# IEPS DATA
#
# Esse codigo trata dados de regiao nacionais vacinas
# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here'), install=T)
# Diretório
dir <- 'Z:/Projects/IEPS Data/Indicadores/Data/'
# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------
# Setar ano final a ser baixado
ano_final <- 2021
# Criar dataframe vazio para armazenar dados
vacinas_br_ano <- NULL
# Loop para tratar os dados
for (ano in 2010:ano_final) {
# Abrir os dados
vacinas_br <- read_xlsx(stringr::str_c(dir, "/01_input_data/A_atencao_basica/A2_cobertura_vacinal/br_",ano,".xlsx"), skip = 3)
# Reshape os dados do formato 'long' para 'wide
vacinas_br <- vacinas_br %>%
filter(!is.na(Total)) %>%
dplyr::select(Imuno, Total) %>%
spread(Imuno, Total) %>%
mutate(ano = ano) # cria coluna de ano
# Juntar anos
vacinas_br_ano <- bind_rows(vacinas_br_ano, vacinas_br)
}
# Preparar pra salvar
vacinas_br_ano <- vacinas_br_ano %>%
# Renomear colunas
dplyr::rename(cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A')%>%
# Mudar separador decimal
dplyr::mutate(across(.cols = !ano, ~stringr::str_replace_all(string = .,
pattern = ',',
replacement = '.') %>%
as.numeric()))%>%
dplyr::select(ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>%
# Considerar coberturas acima de 100% como 100%
dplyr::mutate(across(.cols = !ano, ~case_when(.>100 ~ 100,
T ~.)))
# Salvar base final
readstata13::save.dta13(vacinas_br_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_br.dta'))
# Como função:
limpar_vacinas_br <- function(ano){
readxl::read_xlsx(stringr::str_c(dir, "/01_input_data/A_atencao_basica/A2_cobertura_vacinal/br_",ano,".xlsx"), skip = 3) %>%
filter(!is.na(Total)) %>%
dplyr::select(Imuno, Total) %>%
tidyr::pivot_wider(names_from = Imuno,
values_from = Total) %>%
dplyr::mutate(ano = ano) %>%
dplyr::mutate(across(.cols = !ano, ~stringr::str_replace_all(string = .,
pattern = ',',
replacement = '.') %>%
as.numeric())) %>%
dplyr::mutate(across(.cols = !ano, ~case_when(.>100 ~ 100,
T ~.)))
}
vacinas_br_ano <- list(ano = c(2010:2021)) %>%
purrr::pmap_dfr(.l = ., .f= limpar_vacinas_br) %>%
dplyr::rename(cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A') %>%
dplyr::select(ano, cob_vac_bcg, cob_vac_rota,
cob_vac_menin, cob_vac_pneumo,
cob_vac_polio, cob_vac_tvd1,
cob_vac_penta, cob_vac_hepb, cob_vac_hepa)
# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
# IEPS DATA
#
# Esse codigo trata dados de macroregiao de saude de vacinas
# 0. Configuração---------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here', 'haven','stringi'), install=T)
# Diretório
dir <- "Z:/Projects/IEPS Data/Indicadores/Data/"
# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------
# Setar ano final a ser baixado
ano_final <- 2021
# Criar dataframe vazio para armazenar dados
vacinas_macro_ano <- NULL
# Loop para Tratar os dados
for (ano in 2010:ano_final) {
# Abrir os dados
vacinas_macro <- read_xlsx(stringr::str_c(dir,"01_input_data/A_atencao_basica/A2_cobertura_vacinal/macro_",ano,".xlsx"), skip = 3)
# Adicionar coluna de ano
vacinas_macro <- vacinas_macro%>%
mutate(ano = ano)
# Juntar anos
vacinas_macro_ano <- bind_rows(vacinas_macro_ano, vacinas_macro)
}
# Preparar pra salvar
vacinas_macro_ano <- vacinas_macro_ano %>%
# Renomear colunas
dplyr::rename(macro = "Macrorregião de Saúde",
cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A')%>%
# Retirar linha de totais
filter(!is.na(Total),
macro != 'Total')%>%
# Mudar separador decimal e criar coluna de codigo municipal
mutate(macrorregiao = as.numeric(substr(macro, 1, 4)),
id_estado = as.numeric(substring(macrorregiao, first = 1, last = 2)),
no_macro = substring(macro, 6),
no_macro = str_replace(string = no_macro, pattern = "Âª",replacement =  "ª"),
no_macro = stri_trans_general(tolower(no_macro), "latin-ascii"))%>%
select(macrorregiao, id_estado, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>%
# Considerar coberturas acima de 100% como 100%
mutate_at(.vars=c('cob_vac_bcg','cob_vac_rota','cob_vac_menin','cob_vac_pneumo','cob_vac_polio','cob_vac_tvd1','cob_vac_penta','cob_vac_hepb','cob_vac_hepa'),
.funs=function(x) {ifelse(x>=100,100,x)})
# Salvar base final
write_dta(vacinas_macro_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_macro.dta'))
# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
# IEPS DATA
#
# Esse codigo trata dados de municipios de vacinas
# 0. Configuração---------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here'), install=T)
# Diretório
dir <- 'Z:/Projects/IEPS Data/Indicadores/Data/'
# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------
# Setar ano final a ser baixado
ano_final <- 2021
# Criar dataframe vazio para armazenar dados
vacinas_mun_ano <- NULL
# Loop para tratar os dados
for (ano in 2010:ano_final) {
# Abrir os dados
vacinas_mun <- read_xlsx(stringr::str_c(dir, "/01_input_data/A_atencao_basica/A2_cobertura_vacinal/mun_",ano,".xlsx"), skip = 3)
# Adicionar coluna de ano
vacinas_mun <- vacinas_mun%>%
mutate(ano = ano)
# Juntar anos
vacinas_mun_ano <- bind_rows(vacinas_mun_ano, vacinas_mun)
}
# Preparar pra salvar
vacinas_mun_ano <- vacinas_mun_ano %>%
# Renomear colunas
rename(codmun = "Município",
cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A')%>%
# Retirar linha de totais
filter(!is.na(Total),
codmun != 'Total')%>%
# Mudar separador decimal e criar coluna de codigo municipal
dplyr::mutate(codmun = stringr::str_sub(string= codmun, start = 1, end = 6))%>%
dplyr::select(codmun, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>%
dplyr::mutate(across(.cols = cob_vac_bcg:cob_vac_hepa, ~stringr::str_replace_all(string = .,pattern = ',',replacement = '.') %>% as.numeric()))%>%
# Considerar coberturas acima de 100% como 100%
dplyr::mutate(across(.cols = cob_vac_bcg:cob_vac_hepa, ~case_when(.>100 ~ 100,  T ~.))) %>%
mutate(codmun = as.numeric(codmun))
# Salvar base final
readstata13::save.dta13(vacinas_mun_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_mun.dta'))
# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
# IEPS DATA
#
# Esse codigo trata dados de regiao de saude de vacinas
# 0. Configuração---------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here'), install=T)
# Diretório
dir <- "Z:/Projects/IEPS Data/Indicadores/Data/"
# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------
# Setar ano final a ser baixado
ano_final <- 2021
# Criar dataframe vazio para armazenar dados
vacinas_reg_ano <- NULL
# Loop para tratar os dados
for (ano in 2010:ano_final) {
# Abrir os dados
vacinas_reg <- read_xlsx(stringr::str_c(dir,"01_input_data/A_atencao_basica/A2_cobertura_vacinal/reg_",ano,".xlsx"), skip = 3)
# Adicionar coluna de ano
vacinas_reg <- vacinas_reg%>%
mutate(ano = ano)
# Juntar anos
vacinas_reg_ano <- bind_rows(vacinas_reg_ano, vacinas_reg)
}
# Preparar pra salvar
vacinas_reg_ano <- vacinas_reg_ano %>%
# Renomear colunas
dplyr::rename(regiao = "Região de Saúde (CIR)",
cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A')%>%
# Retirar linha de totais
filter(!is.na(Total),
regiao != 'Total')%>%
# Mudar separador decimal e criar coluna de codigo estadual
mutate(regiao = as.numeric(substr(regiao, 1, 6)),
cob_vac_bcg = as.numeric(gsub(",",".", cob_vac_bcg)),
cob_vac_rota = as.numeric(gsub(",",".", cob_vac_rota)),
cob_vac_menin = as.numeric(gsub(",",".", cob_vac_menin)),
cob_vac_pneumo = as.numeric(gsub(",",".", cob_vac_pneumo)),
cob_vac_polio = as.numeric(gsub(",",".", cob_vac_polio)),
cob_vac_tvd1 = as.numeric(gsub(",",".", cob_vac_tvd1)),
cob_vac_penta = as.numeric(gsub(",",".", cob_vac_penta)),
cob_vac_hepb = as.numeric(gsub(",",".", cob_vac_hepb)),
cob_vac_hepa = as.numeric(gsub(",",".", cob_vac_hepa)))%>%
select(regiao, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>%
# Considerar coberturas acima de 100% como 100%
mutate_at(.vars=c('cob_vac_bcg','cob_vac_rota','cob_vac_menin','cob_vac_pneumo','cob_vac_polio','cob_vac_tvd1','cob_vac_penta','cob_vac_hepb','cob_vac_hepa'),
.funs=function(x) {ifelse(x>=100,100,x)})
# Salvar base final
readstata13::save.dta13(vacinas_reg_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_reg.dta'))
# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
# IEPS DATA
#
# Esse codigo trata dados estaduais de vacinas
# 0. Configuração---------------------------------------
rm(list=ls())
gc()
# Bibliotecas
xfun::pkg_attach(c('tidyverse', 'rvest', 'rlang', 'readxl', 'stringr','here'), install=T)
# Diretório
dir <- 'Z:/Projects/IEPS Data/Indicadores/Data/'
# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------
# Setar ano final a ser baixado
ano_final <- 2021
# Criar dataframe vazio para armazenar dados
vacinas_uf_ano <- NULL
# Loop para tratar os dados
for (ano in 2010:ano_final) {
# Abrir os dados
vacinas_uf <- read_xlsx(stringr::str_c(dir, "/01_input_data/A_atencao_basica/A2_cobertura_vacinal/uf_",ano,".xlsx"), skip = 3)
# Adicionar coluna de ano
vacinas_uf <- vacinas_uf%>%
mutate(ano = ano)
# Juntar anos
vacinas_uf_ano <- bind_rows(vacinas_uf_ano, vacinas_uf)
}
# Preparar pra salvar
vacinas_uf_ano <- vacinas_uf_ano %>%
# Renomear colunas
rename(id_estado = "Unidade da Federação",
cob_vac_bcg = 'BCG',
cob_vac_rota = 'Rotavírus Humano',
cob_vac_menin = 'Meningococo C',
cob_vac_pneumo = 'Pneumocócica',
cob_vac_polio = 'Poliomielite',
cob_vac_tvd1 = 'Tríplice Viral  D1',
cob_vac_penta = 'Penta',
cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
cob_vac_hepa = 'Hepatite A')%>%
# Retirar linha de totais
filter(!is.na(Total),
id_estado != 'Total')%>%
# Mudar separador decimal e criar coluna de codigo estadual
dplyr::mutate(id_estado = stringr::str_sub(string= id_estado, start = 1, end = 2))%>%
dplyr::select(id_estado, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>%
dplyr::mutate(across(.cols = cob_vac_bcg:cob_vac_hepa, ~case_when(.>100 ~ 100,T ~.)))%>%
dplyr::mutate(across(.cols = cob_vac_bcg:cob_vac_hepa, ~stringr::str_replace_all(string = .,pattern = ',', replacement = '.') %>% as.numeric())) %>%
mutate(id_estado = as.numeric(id_estado))
# Salvar base final
readstata13::save.dta13(vacinas_uf_ano, stringr::str_c(dir,'02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_uf.dta'))
# Fim -----------------------------------------------------------------------------------------------------------------------------------------------
