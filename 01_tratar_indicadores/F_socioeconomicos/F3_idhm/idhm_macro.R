# IEPS DATA

# Esse codigo trata os dados de idhm macro

# 0. Config -----------------------------------------------------------------------------------------------------------------------------------------

rm(list=ls())
gc()

# Bibliotecas
library("dplyr")
library("tidyverse")
library("readstata13")
library("haven")
library("readxl")

# Define diretorio
setwd("Z:/Projects/IEPS Data/Indicadores/")

# Input 
path_idhm <- "Data/01_input_data/F_socioeconomicos/F3_idhm/Atlas 2013_municipal, estadual e Brasil.xlsx"
path_pop_idade <- "Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2021.dta" #dados de população por idade
path_reg_saude <- "Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
path_macro_saude <- "Data/02_temp_data/0_auxiliar/Macrorregioes_2022.dta"

# Output
idhm_macro <- "Data/02_temp_data/F_socioeconomicos/F3_idhm/idhm_macro.dta"


# 1.1. Tratar Dados - Abre e Prepara Dados ------------------------------------------------------------------------------------------------

dados_idhm <- read_xlsx(path_idhm, sheet = "MUN 91-00-10") %>%
  filter(ANO == 2010) %>% #filtra do ano mais recente do censo
  rename(ano = ANO,
         uf = UF,
         codmun = Codmun7,
         exp_vida = ESPVIDA)%>%
    rename_all(tolower) %>%
  select(ano, codmun, t_freq5a6, t_fund11a13, t_fund15a17, t_fund18m, t_med18a20, pesotot, peso5, peso6, peso1113, peso1517, peso18, peso1820, exp_vida, rdpc) #renomeia e escolhe as variaveis para salvar
 
   # 1.2. Juntar bases e salva para municipio

# Variavel de codigo municipal com seis digitos

dados_idhm <- dados_idhm %>%
  mutate(codmun = as.numeric(substr(as.character(codmun), 1, 6)))

# Faz merge com dados de unidades geograficas e de populacao por idade

dados_idhm <- dados_idhm %>%
  left_join(select(haven::read_dta(path_reg_saude), ibge, regiao), by = c("codmun" = "ibge")) %>%
  left_join(select(haven::read_dta(path_macro_saude), ibge, macrorregiao), by = c("codmun" = "ibge"))  %>%
  mutate(id_estado = as.numeric(substr(as.character(codmun), 1,2)),
         ano = 2010)  %>%
  left_join(haven::read_dta(path_pop_idade) %>% dplyr::select(codmun, ano), by = c())

# Criar variavel nova (sum pop 5 e 6 anos)

dados_idhm <- dados_idhm %>%
  mutate(peso56 = peso5 + peso6) %>%
  select(-c(peso5, peso6))

#  Agrega populacao por faixas etarias/regiao

dados_idhm <- dados_idhm %>%
  group_by(macrorregiao) %>%
  mutate(peso56_macro = sum(peso56),
         peso1113_macro = sum(peso1113),
         peso1517_macro = sum(peso1517),
         peso1820_macro = sum(peso1820),
         peso18_macro = sum(peso18),
         pesotot_macro = sum(pesotot))


# Cria pct de faixas de populacao

dados_idhm <- dados_idhm %>%
  mutate(pct_peso56 = peso56/peso56_macro,
         pct_peso1113 = peso1113/peso1113_macro,
         pct_peso1517 = peso1517/peso1517_macro,
         pct_peso1820 = peso1820/peso1820_macro,
         pct_peso18 = peso18/peso18_macro,
         pct_pesotot = pesotot/pesotot_macro)
  
# Dividir por 100 variaveis de frequencia escolar

dados_idhm <- dados_idhm %>%
  mutate(t_freq5a6 = t_freq5a6/100,
         t_fund11a13 = t_fund11a13/100,
         t_fund15a17 = t_fund15a17/100,
         t_fund18m = t_fund18m/100,
         t_med18a20 = t_med18a20/100)

# 1.3.Constroi sub-indice para IDHM EDUC

# Cria subindices de Educacao

dados_idhm <- dados_idhm %>%
  mutate(i_escolaridade_aux = t_fund18m * pct_peso18) %>% # subindice escolaridade
  mutate(i_freq_pop_56_aux = t_freq5a6*pct_peso56, # subindice frequencia escolar
         freq_pop_1113_aux = t_fund11a13*pct_peso1113,
         i_freq_pop_1517_aux = t_fund15a17*pct_peso1517,
         i_freq_pop_1820_aux = t_med18a20*pct_peso1820)


# 1.4.Constroi sub-indice para IDHM LONG + Expectativa de Vida

dados_idhm <- dados_idhm %>%
  mutate(exp_vida_aux = exp_vida*pct_pesotot,
         idhm_long_aux = ((exp_vida-25)/(85-25))*pct_pesotot)

# 1.5. Constroi sub-indice para IDHM Renda

dados_idhm <- dados_idhm %>%
mutate(idhm_renda_aux = ((log(rdpc) - log(8)) / (log(4033) - log(8))) * pct_pesotot)

# 1.6. Constroi sub-indice para IDHM

#Colapsa variaveis ao nivel da macrorregiao  de saude

dados_idhm <- dados_idhm %>%
  group_by(macrorregiao) %>%
  summarise(i_escolaridade_aux = sum(i_escolaridade_aux),
    i_freq_pop_56_aux = sum(i_freq_pop_56_aux),
    freq_pop_1113_aux = sum(freq_pop_1113_aux),
    i_freq_pop_1517_aux = sum(i_freq_pop_1517_aux),
    i_freq_pop_1820_aux = sum(i_freq_pop_1820_aux),
    idhm_renda_aux = sum(idhm_renda_aux),
    exp_vida_aux = sum(exp_vida_aux),
    idhm_long_aux = sum(idhm_long_aux)) %>%
  ungroup() 

# Finaliza IDHM-E
dados_idhm <- dados_idhm %>%
  mutate(
  i_freq_prop = (i_freq_pop_56_aux + freq_pop_1113_aux + i_freq_pop_1517_aux + i_freq_pop_1820_aux) / 4,
  idhm_educ = (i_escolaridade_aux ^ (1/3)) * (i_freq_prop ^ (2/3))) %>%
  select(-c(i_escolaridade_aux, i_freq_pop_56_aux, freq_pop_1113_aux, i_freq_pop_1517_aux, 
            i_freq_pop_1820_aux, i_freq_pop_1820_aux)) 

dados_idhm <- dados_idhm %>%
  rename(idhm_renda = idhm_renda_aux,
       idhm_long = idhm_long_aux,
       exp_vida = exp_vida_aux) %>%  # renomear variaveis
  mutate(idhm = (idhm_educ^(1/3))*(idhm_long^(1/3))*(idhm_renda^(1/3))) %>%
  select(macrorregiao, idhm_educ, idhm_long, idhm_renda, idhm, exp_vida)


# Expandir para criar painel 2010-2021 (repete obs de 2010, unico ano disponivel)

dados_idhm <- dados_idhm %>% 
  group_by(macrorregiao) %>% 
  slice(rep(1:n(), each=12))  %>% # repete para os demais anos   
  mutate(ano = c(2010:2021))  # cria variavel de ano

# Salva 
write_dta(dados_idhm, path = idhm_macro) 

