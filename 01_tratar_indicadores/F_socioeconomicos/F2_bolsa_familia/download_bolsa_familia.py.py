import requests
import pandas as pd

res = requests.get('https://aplicacoes.mds.gov.br/sagi/servicos/misocial?q=*&fq=tipo_s:mes_mu&wt=csv&fl=ibge:codigo_ibge,anomes:anomes_s,qtd_familias_beneficiarias_bolsa_familia,valor_repassado_bolsa_familia&rows=10000000&sort=anomes_s%20asc,%20codigo_ibge%20asc')

bolsa = res.text.strip()

with open("bolsa_familia_2020.csv","w") as file:
    file.write(bolsa + "\n")

print(bolsa)
