## IEPS DATA
#
# Esse codigo baixa e trata dados do bolsa familia



# 0. Config --------------------------------------------------------------------
rm(list=ls())
gc()

# Bibliotecas
xfun::pkg_attach(c('dplyr', 'tidyr','readr', 'haven','purrr','stringr', 'readstata13'), install=T)

# Diretorio
path <- 'Z:/Projects/IEPS Data/Indicadores/Data/'

# Input
path_demografia <- stringr::str_c(path,'02_temp_data/G_demografia/pop_tabnet_idade_2010_2021.dta')
path_reg   <- stringr::str_c(path,'02_temp_data/0_auxiliar/Regioes_2022.dta')
path_macro <-stringr::str_c(path,'02_temp_data/0_auxiliar/Macrorregioes_2022.dta')
path_muns       <- stringr::str_c(path,'02_temp_data/0_auxiliar/municipios.dta')

# 1. Baixar dados --------------------------------------------------------------
pbf_download <- function(ano){
  readr::read_csv(file = stringr::str_c('https://aplicacoes.mds.gov.br/sagi/servicos/misocial?fq=anomes_s:',ano,'*&fq=tipo_s:mes_mu&wt=csv&q=*&fl=ibge:codigo_ibge,anomes:anomes_s,qtd_familias_beneficiarias_bolsa_familia,valor_repassado_bolsa_familia&rows=10000000&sort=anomes_s%20asc,%20codigo_ibge%20asc'))
}

pbf <-  tibble(ano = 2010:2022) %>%
  purrr::map_df(.x= .$ano, .f=~pbf_download(ano = .x))

# 2. Salvar --------------------------------------------------------------------
readr::write_csv(pbf,stringr::str_c(path,'01_input_data/F_socioeconomicos/F2_bolsa_familia/pbf.csv'))


# 3. Le arquivos --------------------------------------------------------------------------------------------------------------------------------------------
reg   <-read.dta13(path_reg)
macro <-read.dta13(path_macro)
muns  <-read.dta13(path_muns) %>% 
  dplyr::select(id_munic_6, estado_abrev, estado)

# 4. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

# Abrir dados populacionais
pop       <- haven::read_stata(path_demografia) %>% 
  dplyr::select(ano, codmun, pop)

# Preparar base de bolsa familia
pbf_temp_valor <- pbf %>% 
  
  # Criar novas variaveis
  mutate(ano = as.numeric(str_sub(anomes, 1,4)),
         mes = str_sub(anomes, 5, 6)) %>%
  
  # Selecionar variaveis
  select(codmun = ibge, ano,
         valor = valor_repassado_bolsa_familia) %>% 
  
  group_by(codmun, ano) %>% 
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>%  # soma por ano o numero de beneficiarios e valor pago
  left_join(pop, by = c("codmun", "ano"))   # junta dados de populacao municipal

# Preparar base de bolsa familia
pbf_temp_benef_21 <- pbf %>% 
  
  # Criar novas variaveis
  mutate(ano = as.numeric(str_sub(anomes, 1,4)),
         mes = as.numeric(str_sub(anomes, 5, 6))) %>%
  
  # Selecionar variaveis
  select(codmun = ibge, ano, mes, num_familias_bf = qtd_familias_beneficiarias_bolsa_familia,
         valor = valor_repassado_bolsa_familia) %>% 
  
  filter(mes == 10 & ano ==2021) %>% 
  dplyr::select(codmun, ano, num_familias_bf)

# Preparar base de bolsa familia
pbf_temp_benef_20 <- pbf %>% 
  
  # Criar novas variaveis
  mutate(ano = as.numeric(str_sub(anomes, 1,4)),
         mes = as.numeric(str_sub(anomes, 5, 6))) %>%
  
  # Selecionar variaveis
  select(codmun = ibge, ano, mes, num_familias_bf = qtd_familias_beneficiarias_bolsa_familia,
         valor = valor_repassado_bolsa_familia) %>% 
  
  filter(mes == 12 & ano < 2021) %>% 
  dplyr::select(codmun, ano, num_familias_bf)

pbf_temp_benef <- pbf_temp_benef_20 %>% 
  bind_rows(pbf_temp_benef_21)

pbf_temp <- pbf_temp_valor %>% 
  full_join(pbf_temp_benef, by = c("codmun", "ano"))

# Salvar dados municipais
pbf_mun <- pbf_temp %>% 
  mutate(gasto_pbf_pc = valor/pop) %>% 
  dplyr::select(codmun, ano, num_familias_bf, gasto_pbf_pc)

# Salvar 
readstata13::save.dta13(pbf_mun, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_mun.dta'))

# Parte 3. Agregar para niveis de regiao de saude -----------------------------------------------------------------------------------------------------------------------------
# 3.1) Região de Saúde
pbf_reg <- pbf_temp %>% 
  left_join(reg, by = c('codmun'='ibge')) %>% # junta informacao de regiao de saude 
  group_by(regiao, ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(gasto_pbf_pc = valor/pop) %>% 
  dplyr::select(regiao, ano, num_familias_bf, gasto_pbf_pc)

# Salvar 
readstata13::save.dta13(pbf_reg, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_reg.dta'))


# 3.2) Macrorregião de Saúde
pbf_macro <- pbf_temp %>% 
  left_join(macro, by = c('codmun'='ibge')) %>% # junta informacao de macrorregiao de saude 
  group_by(macrorregiao, ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(gasto_pbf_pc = valor/pop) %>% 
  dplyr::select(macrorregiao, ano, num_familias_bf, gasto_pbf_pc)

# Salvar 
readstata13::save.dta13(pbf_macro, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_macro.dta'))

# 3.3) Estado
pbf_uf <- pbf_temp %>% 
  left_join(muns, by = c('codmun'='id_munic_6')) %>% # junta informacao de regiao de saude 
  group_by(estado_abrev, ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(gasto_pbf_pc = valor/pop) %>% 
  dplyr::select(estado_abrev, ano, num_familias_bf, gasto_pbf_pc)

# Salvar 
write_dta(pbf_uf, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_uf.dta'))

# 3.4) Nível nacional
pbf_br <- pbf_temp %>% 
  group_by(ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(gasto_pbf_pc = valor/pop) %>% 
  dplyr::select(ano,num_familias_bf, gasto_pbf_pc)

# Salvar 
readstata13::save.dta13(pbf_br, stringr::str_c(path,'02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_br.dta'))


# FIM -----------------------------------------------------------------------------------------------------------------------------------------------