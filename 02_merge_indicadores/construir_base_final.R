# IEPS DATA
#
# Esse codigo agrega indicadores do IEPS Data nos diferentes niveis geograficos e gera base final de indicadores



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())


# Bibliotecas
xfun::pkg_attach(c('stringr', 'stringi','tidyverse', 'readstata13', 'haven', 'zoo'), install=T)


# Setar diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/')

# Input

# UNIDADES GEOGRAFICAS
path_reg   <-'02_temp_data/0_auxiliar/Regioes_2022.dta'
path_macro <-'02_temp_data/0_auxiliar/Macrorregioes_2022.dta'
path_muns  <-'02_temp_data/0_auxiliar/municipios.dta'
path_capitais <- "Z:/Master/Geo/capitals/capitals.dta"


# BLOCO A - ATENCAO BASICA
path_cobertura_ab_mun    <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_mun.dta'
path_cobertura_ab_reg    <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_reg.dta'
path_cobertura_ab_macro  <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_macro.dta'
path_cobertura_ab_uf     <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_uf.dta'
path_cobertura_ab_br     <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_br.dta'

path_cobertura_acs_mun   <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_mun.dta'
path_cobertura_acs_reg   <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_reg.dta'
path_cobertura_acs_macro <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_macro.dta'
path_cobertura_acs_uf    <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_uf.dta'
path_cobertura_acs_br    <- '02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_br.dta'

path_vacinas_mun   <- '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_mun.dta'
path_vacinas_reg   <- '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_reg.dta'
path_vacinas_macro <- '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_macro.dta'
path_vacinas_uf    <- '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_uf.dta'
path_vacinas_br    <- '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_br.dta'

path_prenatal_mun     <- '02_temp_data/A_atencao_basica/A3_prenatal/prenatal_mun.dta'
path_prenatal_reg     <- '02_temp_data/A_atencao_basica/A3_prenatal/prenatal_reg.dta'
path_prenatal_macro   <- '02_temp_data/A_atencao_basica/A3_prenatal/prenatal_macro.dta'
path_prenatal_uf      <- '02_temp_data/A_atencao_basica/A3_prenatal/prenatal_uf.dta'
path_prenatal_br      <- '02_temp_data/A_atencao_basica/A3_prenatal/prenatal_br.dta'


# BLOCO B - MORTALIDADE E MORBIDADE
path_mort_inf_mun     <- '02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_mun.dta'
path_mort_inf_reg     <- '02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_reg.dta'
path_mort_inf_macro   <- '02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_macro.dta'
path_mort_inf_uf      <- '02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_uf.dta'
path_mort_inf_br      <- '02_temp_data/B_morbidade_mortalidade/B1_mortinf/mortinf_ibge_br.dta'

path_mort_mun         <- '02_temp_data/B_morbidade_mortalidade/B2_mort/sim_mun_adj.dta'
path_mort_reg         <- '02_temp_data/B_morbidade_mortalidade/B2_mort/sim_reg_adj.dta'
path_mort_macro       <- '02_temp_data/B_morbidade_mortalidade/B2_mort/sim_macro_adj.dta'
path_mort_uf          <- '02_temp_data/B_morbidade_mortalidade/B2_mort/sim_uf_adj.dta'
path_mort_br          <- '02_temp_data/B_morbidade_mortalidade/B2_mort/sim_br_adj.dta'

path_hosp_mun         <- '02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_mun.dta'
path_hosp_reg         <- '02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_reg.dta'
path_hosp_macro       <- '02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_macro.dta'
path_hosp_uf          <- '02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_uf.dta'
path_hosp_br          <- '02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_br.dta'


# BLOCO C - RECURSOS
path_recursos_mun     <- '02_temp_data/C_recursos/recursos_mun.dta' 
path_recursos_reg     <- '02_temp_data/C_recursos/recursos_reg.dta' 
path_recursos_macro   <- '02_temp_data/C_recursos/recursos_macro.dta' 
path_recursos_uf      <- '02_temp_data/C_recursos/recursos_uf.dta' 
path_recursos_br      <- '02_temp_data/C_recursos/recursos_br.dta' 

# BLOCO D - SAUDE SUPLEMENTAR
path_leitos_mun       <- '02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_mun.dta'
path_leitos_reg       <- '02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_reg.dta'
path_leitos_macro     <- '02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_macro.dta'
path_leitos_uf        <- '02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_uf.dta'
path_leitos_br        <- '02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_br.dta'

path_cobertura_ans_mun   <- '02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_mun.dta'
path_cobertura_ans_reg   <- '02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_reg.dta'
path_cobertura_ans_macro <- '02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_macro.dta'
path_cobertura_ans_uf    <- '02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_uf.dta'
path_cobertura_ans_br    <- '02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_br.dta'

# BLOCO E - GASTOS
path_gastos_mun        <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_mun_def.dta'
path_gastos_mun_reg    <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_mun_reg_def.dta'
path_gastos_mun_macro  <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_mun_macro_def.dta'
path_gastos_mun_uf     <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_mun_uf_def.dta'
path_gastos_mun_br     <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_mun_br_def.dta'

path_gastos_uf         <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_uf_uf_def.dta' 
path_gastos_uf_br      <-'Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/E_gastos/siops_uf_br_def.dta' 

# BLOCO F - SOCIOECONOMICOS
path_ideb_mun          <- '02_temp_data/F_socioeconomicos/F1_ideb/ideb_mun.dta'
path_ideb_reg          <- '02_temp_data/F_socioeconomicos/F1_ideb/ideb_reg.dta'
path_ideb_macro        <- '02_temp_data/F_socioeconomicos/F1_ideb/ideb_macro.dta'
path_ideb_uf           <- '02_temp_data/F_socioeconomicos/F1_ideb/ideb_uf.dta'
path_ideb_br           <- '02_temp_data/F_socioeconomicos/F1_ideb/ideb_br.dta'


path_bolsa_familia_mun    <- '02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_mun_def.dta'
path_bolsa_familia_reg    <- '02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_reg_def.dta'
path_bolsa_familia_macro  <- '02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_macro_def.dta'
path_bolsa_familia_uf     <- '02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_uf_def.dta'
path_bolsa_familia_br     <- '02_temp_data/F_socioeconomicos/F2_bolsa_familia/bolsa_familia_br_def.dta'

path_idhm_mun             <- '02_temp_data/F_socioeconomicos/F3_idhm/idhm_mun.dta'
path_idhm_reg             <- '02_temp_data/F_socioeconomicos/F3_idhm/idhm_reg.dta'
path_idhm_macro           <- '02_temp_data/F_socioeconomicos/F3_idhm/idhm_macro.dta'
path_idhm_uf              <- '02_temp_data/F_socioeconomicos/F3_idhm/idhm_uf.dta'
path_idhm_br              <- '02_temp_data/F_socioeconomicos/F3_idhm/idhm_br.dta'


path_censo_mun         <- '02_temp_data/F_socioeconomicos/F4_censo/censo_mun.dta'
path_censo_reg         <- '02_temp_data/F_socioeconomicos/F4_censo/censo_reg.dta'
path_censo_macro       <- '02_temp_data/F_socioeconomicos/F4_censo/censo_macro.dta'
path_censo_uf          <- '02_temp_data/F_socioeconomicos/F4_censo/censo_uf.dta'
path_censo_br          <- '02_temp_data/F_socioeconomicos/F4_censo/censo_br.dta'

path_pib_mun           <- '02_temp_data/F_socioeconomicos/F5_pib/pib_def_mun.dta'
path_pib_reg           <- '02_temp_data/F_socioeconomicos/F5_pib/pib_def_reg.dta'
path_pib_macro         <- '02_temp_data/F_socioeconomicos/F5_pib/pib_def_macro.dta'
path_pib_uf            <- '02_temp_data/F_socioeconomicos/F5_pib/pib_def_uf.dta'
path_pib_br            <- '02_temp_data/F_socioeconomicos/F5_pib/pib_def_br.dta'

path_renda_mun        <- '02_temp_data/F_socioeconomicos/F6_renda/renda_mun.dta'  
path_renda_reg        <- '02_temp_data/F_socioeconomicos/F6_renda/renda_reg.dta'  
path_renda_macro      <- '02_temp_data/F_socioeconomicos/F6_renda/renda_macro.dta'  
path_renda_uf         <- '02_temp_data/F_socioeconomicos/F6_renda/renda_uf.dta'  
path_renda_br         <- '02_temp_data/F_socioeconomicos/F6_renda/renda_br.dta'  


# BLOCO G - DEMOGRAFIA
path_pop_idade_mun     <-  '02_temp_data/G_demografia/pop_idade_2010_2022.dta'
path_pop_idade_reg     <-  '02_temp_data/G_demografia/pop_idade_2010_2022_reg.dta'
path_pop_idade_macro   <-  '02_temp_data/G_demografia/pop_idade_2010_2022_macro.dta'
path_pop_idade_uf      <-  '02_temp_data/G_demografia/pop_idade_2010_2022_uf.dta'
path_pop_idade_br      <-  '02_temp_data/G_demografia/pop_idade_2010_2022_br.dta'


path_pop_idade_sexo_mun <-'02_temp_data/G_demografia/pop_tabnet_idade_sexo_2010_2022_mun.dta'
path_pop_idade_sexo_reg <-'02_temp_data/G_demografia/pop_tabnet_idade_sexo_2010_2022_reg.dta'
path_pop_idade_sexo_macro <-'02_temp_data/G_demografia/pop_tabnet_idade_sexo_2010_2022_macro.dta'
path_pop_idade_sexo_uf <-'02_temp_data/G_demografia/pop_tabnet_idade_sexo_2010_2022_uf.dta'
path_pop_idade_sexo_br <-'02_temp_data/G_demografia/pop_tabnet_idade_sexo_2010_2022_br.dta'


# Output
sf_mun    <-'03_output_data/mun_data.csv'
sf_reg    <-'03_output_data/reg_data.csv'
sf_macro  <-'03_output_data/macro_data.csv'
sf_state  <-'03_output_data/state_data.csv'
sf_brazil <-'03_output_data/brazil_data.csv'



# 1. Ler Dados -----------------------------------------------------------------------------------------------------------------------------

# UNIDADES GEOGRAFICAS
reg   <- read.dta13(path_reg) %>%  # regiao de saude
  dplyr::select(ibge, regiao, no_regiao)
macro <-read.dta13(path_macro) %>% # macroregiao de saude
  dplyr::select(ibge, macrorregiao, no_macro)
muns <- read.dta13(path_muns) %>%  # municipio 
  dplyr::select(id_munic_7, id_munic_6, municipio, id_estado, estado_abrev, estado) %>% # tambem contem dados de estado
  dplyr::rename(codmun=id_munic_6,
                nomemun=municipio) 
capitais <- read.dta13(path_capitais) %>% 
  mutate(capital = 1) %>% 
  dplyr::select(codmun, capital)


# BLOCO A - ATENCAO BASICA
cobertura_ab_mun    <- read.dta13(path_cobertura_ab_mun) 
cobertura_ab_reg    <- read.dta13(path_cobertura_ab_reg) 
cobertura_ab_macro  <- read.dta13(path_cobertura_ab_macro) 
cobertura_ab_uf     <- read.dta13(path_cobertura_ab_uf) 
cobertura_ab_br     <- read.dta13(path_cobertura_ab_br) 

cobertura_acs_mun   <- read.dta13(path_cobertura_acs_mun)
cobertura_acs_reg   <- read.dta13(path_cobertura_acs_reg)
cobertura_acs_macro <- read.dta13(path_cobertura_acs_macro)
cobertura_acs_uf    <- read.dta13(path_cobertura_acs_uf)
cobertura_acs_br    <- read.dta13(path_cobertura_acs_br)

vacinas_mun   <- read.dta13(path_vacinas_mun) 
vacinas_reg   <- read.dta13(path_vacinas_reg) 
vacinas_macro <- read.dta13(path_vacinas_macro) 
vacinas_uf    <- read.dta13(path_vacinas_uf) 
vacinas_br    <- read.dta13(path_vacinas_br) 

prenatal_mun     <- read.dta13(path_prenatal_mun)
prenatal_reg     <- read.dta13(path_prenatal_reg)
prenatal_macro   <- read.dta13(path_prenatal_macro)
prenatal_uf      <- read.dta13(path_prenatal_uf)
prenatal_br      <- read.dta13(path_prenatal_br)


# BLOCO B - MORTALIDADE E MORBIDADE
mort_inf_mun    <- read.dta13(path_mort_inf_mun)
mort_inf_reg    <- read.dta13(path_mort_inf_reg)
mort_inf_macro  <- read.dta13(path_mort_inf_macro)
mort_inf_uf     <- read.dta13(path_mort_inf_uf)
mort_inf_br     <- read.dta13(path_mort_inf_br)

mort_mun        <- read.dta13(path_mort_mun)
mort_reg        <- read.dta13(path_mort_reg)
mort_macro      <- read.dta13(path_mort_macro)
mort_uf         <- read.dta13(path_mort_uf)
mort_br         <- read.dta13(path_mort_br)

hosp_mun       <- read.dta13(path_hosp_mun)
hosp_reg       <- read.dta13(path_hosp_reg)
hosp_macro     <- read.dta13(path_hosp_macro)
hosp_uf        <- read.dta13(path_hosp_uf)
hosp_br        <- read.dta13(path_hosp_br)


# BLOCO C - RECURSOS
recursos_mun    <- read.dta13(path_recursos_mun)
recursos_reg    <- read.dta13(path_recursos_reg)
recursos_macro  <- read.dta13(path_recursos_macro)
recursos_uf     <- read.dta13(path_recursos_uf)
recursos_br     <- read.dta13(path_recursos_br)

# BLOCO D - SAUDE SUPLEMENTAR
leitos_mun       <- read.dta13(path_leitos_mun) 
leitos_reg       <- read.dta13(path_leitos_reg)
leitos_macro     <- read.dta13(path_leitos_macro)
leitos_uf        <- read.dta13(path_leitos_uf)
leitos_br        <- read.dta13(path_leitos_br)

cobertura_ans_mun   <- read.dta13(path_cobertura_ans_mun)
cobertura_ans_reg   <- read.dta13(path_cobertura_ans_reg)
cobertura_ans_macro <- read.dta13(path_cobertura_ans_macro)
cobertura_ans_uf    <- read.dta13(path_cobertura_ans_uf)
cobertura_ans_br    <- read.dta13(path_cobertura_ans_br)


# BLOCO E - GASTOS 
gastos_mun        <- read.dta13(path_gastos_mun) 
gastos_mun_reg    <- read.dta13(path_gastos_mun_reg)
gastos_mun_macro  <- read.dta13(path_gastos_mun_macro)
gastos_mun_uf     <- read.dta13(path_gastos_mun_uf)
gastos_mun_br     <- read.dta13(path_gastos_mun_br)

gastos_uf      <- read.dta13(path_gastos_uf)
gastos_uf_br   <- read.dta13(path_gastos_uf_br)


# BLOCO F - SOCIOECONOMICOS
ideb_mun         <- read.dta13(path_ideb_mun)
ideb_reg         <- read.dta13(path_ideb_reg)
ideb_macro       <- read.dta13(path_ideb_macro)
ideb_uf          <- read.dta13(path_ideb_uf)
ideb_br          <- read.dta13(path_ideb_br)

bolsa_familia_mun   <- read.dta13(path_bolsa_familia_mun) 
bolsa_familia_reg   <- read.dta13(path_bolsa_familia_reg)
bolsa_familia_macro <- read.dta13(path_bolsa_familia_macro)
bolsa_familia_uf    <- read.dta13(path_bolsa_familia_uf)
bolsa_familia_br    <- read.dta13(path_bolsa_familia_br)

idhm_mun        <- read.dta13(path_idhm_mun) 
idhm_reg        <- read.dta13(path_idhm_reg) 
idhm_macro      <- read.dta13(path_idhm_macro) 
idhm_uf         <- read.dta13(path_idhm_uf) 
idhm_br         <- read.dta13(path_idhm_br) 

censo_mun        <- read.dta13(path_censo_mun) 
censo_reg        <- read.dta13(path_censo_reg)
censo_macro      <- read.dta13(path_censo_macro)
censo_uf         <- read.dta13(path_censo_uf)
censo_br         <- read.dta13(path_censo_br)

pib_mun          <- read.dta13(path_pib_mun) 
pib_reg          <- read.dta13(path_pib_reg)
pib_macro        <- read.dta13(path_pib_macro)
pib_uf           <- read.dta13(path_pib_uf)
pib_br           <- read.dta13(path_pib_br)

renda_mun       <- read.dta13(path_renda_mun)
renda_reg       <- read.dta13(path_renda_reg)
renda_macro     <- read.dta13(path_renda_macro)
renda_uf        <- read.dta13(path_renda_uf)
renda_br        <- read.dta13(path_renda_br)

# BLOCO G - DEMOGRAFIA
pop_idade_mun     <- read.dta13(path_pop_idade_mun) 
pop_idade_reg     <- read.dta13(path_pop_idade_reg)
pop_idade_macro   <- read.dta13(path_pop_idade_macro)
pop_idade_uf      <- read.dta13(path_pop_idade_uf)
pop_idade_br      <- read.dta13(path_pop_idade_br)

pop_idade_sexo_mun   <- read.dta13(path_pop_idade_sexo_mun)
pop_idade_sexo_reg   <- read.dta13(path_pop_idade_sexo_reg)
pop_idade_sexo_macro <- read.dta13(path_pop_idade_sexo_macro)
pop_idade_sexo_uf    <- read.dta13(path_pop_idade_sexo_uf)
pop_idade_sexo_br    <- read.dta13(path_pop_idade_sexo_br)

# Remover caminho dos arquivos
rm(list=ls(pattern="^path_"))



# 2. Unidades Geograficas ---------------------------------------------------------------------------------------------------------------------------

# Criar data frame com dados de unidades geograficas identificados, anos 2010:2022
esqueleto_geografia <- muns %>%
  merge(data.frame(ano = c(seq(2010,2022))), all=T) %>% # cria coluna de anos 
  left_join(reg,   by = c('codmun'='ibge')) %>% # junta informacao de regiao de saude
  left_join(macro, by = c('codmun'='ibge')) %>% # junta informacao de macrorregiao de saude
  mutate(regiao_geo = ifelse(estado_abrev %in% c("AC", "AM", "RO", "RR", "PA", "AP", "TO"), "Norte",
                            ifelse(estado_abrev %in% c("MA", "PI", "CE", "RN", "PB", "PE", "AL", "SE", "BA"), "Nordeste",
                                   ifelse(estado_abrev %in% c("MG", "RJ", "SP", "ES"), "Sudeste",
                                          ifelse(estado_abrev %in% c("MT", "GO", "MS", "DF"), "Centro-Oeste", "Sul"))))) %>% 
  left_join(capitais, by = "codmun") %>% 
  mutate(capital = ifelse(is.na(capital)==TRUE,0, capital))
  
  


# 3. Merge Municipio -----------------------------------------------------------------------------------------------------------------------------


# Municipio 
dados_mun <- esqueleto_geografia %>%
  
  # Juntar dados por bloco
  
  # BLOCO A - ATENCAO BASICA
  left_join(cobertura_ab_mun,  by = c('codmun','ano')) %>%
  left_join(cobertura_acs_mun, by = c('codmun','ano')) %>% 
  left_join(vacinas_mun,       by = c('codmun','ano')) %>% 
  left_join(prenatal_mun,      by = c('codmun','ano'))  %>% 
  
  # BLOCO B - MORTALIDADE E MORBIDADE
  left_join(mort_inf_mun,      by = c('codmun','ano')) %>% 
  left_join(mort_mun,          by = c('codmun','ano')) %>% 
  left_join(hosp_mun,          by = c('codmun','ano')) %>% 
  
  # BLOCO C - RECURSOS
  left_join(recursos_mun,      by = c('codmun','ano'))  %>% 
  
  # BLOCO D - SAUDE SUPLEMENTAR
  left_join(leitos_mun,         by = c('codmun','ano'))  %>% 
  left_join(cobertura_ans_mun,  by = c('codmun','ano'))  %>% 
  
  # BLOCO E - GASTOS
  left_join(gastos_mun,  by = c('codmun','ano'))  %>% 
  
  # BLOCO F - SOCIOECONOMICOS
  left_join(ideb_mun,           by = c('id_munic_7','ano'))  %>% 
  left_join(bolsa_familia_mun,  by = c('codmun','ano'))  %>% 
  left_join(idhm_mun,           by = c('codmun','ano'))  %>% 
  left_join(censo_mun,          by = c('codmun','ano'))  %>%
  left_join(pib_mun,            by = c('codmun','ano'))  %>%
  left_join(renda_mun,          by = c('codmun','ano'))  %>%
  
  # BLOCO G - DEMOGRAFIA
  left_join(pop_idade_mun,  by = c('codmun','ano'))  %>% 
  left_join(pop_idade_sexo_mun,  by = c('codmun','ano')) %>% 
  
  dplyr::select(ano, regiao_geo, id_munic_7, codmun, capital, -estado, everything())



# 4. Merge Regiao de Saude -----------------------------------------------------------------------------------------------------------------------------

dados_reg <- esqueleto_geografia %>% 
  
  distinct(ano, regiao, no_regiao,id_estado, estado_abrev, estado, regiao_geo, .keep_all = FALSE) %>% 
  
  left_join(cobertura_ab_reg,  by = c('regiao','ano')) %>%
  left_join(cobertura_acs_reg, by = c('regiao','ano')) %>% 
  left_join(vacinas_reg,       by = c('regiao','ano')) %>% 
  left_join(prenatal_reg,      by = c('regiao','ano'))  %>% 
  
  # BLOCO B - MORTALIDADE E MORBIDADE
  left_join(mort_inf_reg,      by = c('regiao','ano')) %>% # #ok
  left_join(mort_reg,          by = c('regiao','ano')) %>% 
  left_join(hosp_reg,          by = c('regiao','ano'))  %>% 
  
  # BLOCO C - RECURSOS
  left_join(recursos_reg,      by = c('regiao','ano'))  %>% 
  
  # BLOCO D - SAUDE SUPLEMENTAR
  left_join(leitos_reg,         by = c('regiao','ano'))  %>% 
  left_join(cobertura_ans_reg,  by = c('regiao','ano'))   %>% 
  
  # BLOCO E - GASTOS
  left_join(gastos_mun_reg,  by = c('regiao','ano'))  %>% #ok
  
  # BLOCO F - SOCIOECONOMICOS
  left_join(ideb_reg,           by = c('regiao','ano'))  %>%  # ok
  left_join(bolsa_familia_reg,  by = c('regiao','ano'))  %>%  # ok
  left_join(idhm_reg,           by = c('regiao','ano'))  %>% 
  left_join(censo_reg,          by = c('regiao','ano'))  %>%
  left_join(pib_reg,            by = c('regiao','ano'))  %>%
  left_join(renda_reg,          by = c('regiao','ano'))   %>%
  
  # BLOCO G - DEMOGRAFIA
  left_join(pop_idade_reg,       by = c('regiao','ano'))  %>% # ok
  left_join(pop_idade_sexo_reg,  by = c('regiao','ano'))  # ok



# 5. Merge Macrorregiao de Saude -----------------------------------------------------------------------------------------------------------------------------

dados_macro <- esqueleto_geografia %>% 
  
  distinct(ano, macrorregiao, no_macro, id_estado, estado_abrev, estado, regiao_geo, .keep_all = FALSE) %>% 
  mutate(no_macro_temp = no_macro, # cria uma var temporaria com upper/lower case normais para usar depois de fazer o merge
         no_macro = str_trim(stri_trans_general(tolower(no_macro), "latin-ascii"))) %>% 
  
  left_join(cobertura_ab_macro,  by = c('macrorregiao','ano')) %>%
  left_join(cobertura_acs_macro, by = c('macrorregiao','ano')) %>% 
  left_join(vacinas_macro,       by = c("macrorregiao","id_estado","ano")) %>% 
  left_join(prenatal_macro,      by = c('macrorregiao','ano'))  %>% 
  
  # BLOCO B - MORTALIDADE E MORBIDADE
  left_join(mort_inf_macro,      by = c('macrorregiao','ano')) %>% 
  left_join(mort_macro,          by = c('macrorregiao','ano')) %>% 
  left_join(hosp_macro,          by = c('macrorregiao','ano'))  %>% 
  
  # BLOCO C - RECURSOS
  left_join(recursos_macro,      by = c('macrorregiao','ano'))  %>% 
  
  # BLOCO D - SAUDE SUPLEMENTAR
  left_join(leitos_macro,         by = c('macrorregiao','ano'))  %>% 
  left_join(cobertura_ans_macro,  by = c('macrorregiao','ano'))   %>% 
  
  # BLOCO E - GASTOS
  left_join(gastos_mun_macro,  by = c('macrorregiao','ano'))   %>% 
  
  # BLOCO F - SOCIOECONOMICOS
  left_join(ideb_macro,           by = c('macrorregiao','ano'))  %>% 
  left_join(bolsa_familia_macro,  by = c('macrorregiao','ano'))  %>% 
  left_join(idhm_macro,           by = c('macrorregiao','ano'))  %>% 
  left_join(censo_macro,          by = c('macrorregiao','ano'))  %>%
  left_join(pib_macro,            by = c('macrorregiao','ano')) %>%
  left_join(renda_macro,          by = c('macrorregiao','ano'))  %>%
  
  # BLOCO G - DEMOGRAFIA
  left_join(pop_idade_macro,  by = c('macrorregiao','ano'))  %>% 
  left_join(pop_idade_sexo_macro,  by = c('macrorregiao','ano')) #%>% 
  
 # dplyr::select(-no_macro) %>% 
#  rename(no_macro = no_macro_temp)
  
#dplyr::select(-macro_nome) # dropa variavel de nome igual a no_macro criada para match com dados vacinais


# 6. Merge UF -----------------------------------------------------------------------------------------------------------------------------

dados_uf <- esqueleto_geografia %>% 
  
  distinct(ano, id_estado, estado_abrev, estado, regiao_geo, .keep_all = FALSE) %>% 
  
  left_join(cobertura_ab_uf,  by = c('id_estado','ano')) %>%
  left_join(cobertura_acs_uf, by = c('id_estado','ano')) %>% 
  left_join(vacinas_uf,       by = c('id_estado','ano')) %>% 
  left_join(prenatal_uf,      by = c('id_estado','ano'))  %>% 
  
  # BLOCO B - MORTALIDADE E MORBIDADE
  left_join(mort_inf_uf,      by = c('estado_abrev' = 'UF','ano')) %>% 
  left_join(mort_uf,          by = c('id_estado','ano')) %>% 
  left_join(hosp_uf,          by = c('id_estado','ano'))  %>% 
  
  # BLOCO C - RECURSOS
  left_join(recursos_uf,      by = c('id_estado','ano'))  %>% 
  
  # BLOCO D - SAUDE SUPLEMENTAR
  left_join(leitos_uf,         by = c('id_estado','ano'))  %>% 
  left_join(cobertura_ans_uf,  by = c('id_estado','ano'))   %>% 
  
  # BLOCO E - GASTOS
  left_join(gastos_mun_uf,  by = c('id_estado','ano'))   %>% 
  left_join(gastos_uf,  by = c('estado' = 'uf','ano'))   %>% 
  
  
  # BLOCO F - SOCIOECONOMICOS
  left_join(ideb_uf,           by = c('estado','ano'))  %>% 
  left_join(bolsa_familia_uf,  by = c('estado_abrev','ano'))   %>% 
  left_join(idhm_uf,           by = c('id_estado','ano'))  %>% 
  left_join(censo_uf,          by = c('id_estado','ano'))  %>%
  left_join(pib_uf,            by = c('id_estado','ano'))  %>%
  left_join(renda_uf,          by = c('id_estado','ano'))  %>%
    
  # BLOCO G - DEMOGRAFIA
  left_join(pop_idade_uf,  by = c('id_estado','ano'))  %>% 
  left_join(pop_idade_sexo_uf,  by = c('id_estado','ano')) 




# 7. Merge BR -----------------------------------------------------------------------------------------------------------------------------

dados_br <- esqueleto_geografia %>% 
  
  distinct(ano, .keep_all = FALSE) %>% 
  
  left_join(cobertura_ab_br,  by = c('ano')) %>%
  left_join(cobertura_acs_br, by = c('ano')) %>% 
  left_join(vacinas_br,       by = c('ano')) %>% 
  left_join(prenatal_br,      by = c('ano'))  %>% 
  
  # BLOCO B - MORTALIDADE E MORBIDADE
  left_join(mort_inf_br,          by = c('ano')) %>% 
  left_join(mort_br,          by = c('ano')) %>% 
  left_join(hosp_br,          by = c('ano'))  %>% 
  
  # BLOCO C - RECURSOS
  left_join(recursos_br,      by = c('ano'))  %>% 
  
  # BLOCO D - SAUDE SUPLEMENTAR
  left_join(leitos_br,         by = c('ano'))  %>% 
  left_join(cobertura_ans_br,  by = c('ano'))   %>% 
  
  # BLOCO E - GASTOS
  left_join(gastos_mun_br,  by = c('ano'))   %>% 
 left_join(gastos_uf_br,    by = c('ano'))   %>% 
  
  
  # BLOCO F - SOCIOECONOMICOS
  left_join(ideb_br,           by = c('ano'))  %>% 
  left_join(bolsa_familia_br,  by = c('ano'))  %>% 
  left_join(idhm_br,           by = c('ano'))  %>% 
  left_join(censo_br,          by = c('ano'))  %>%
  left_join(pib_br,            by = c('ano'))  %>%
  left_join(renda_br,          by = c('ano'))  %>%
    
  # BLOCO G - DEMOGRAFIA
  left_join(pop_idade_br,  by = c('ano'))  %>% 
  left_join(pop_idade_sexo_br,  by = c('ano')) 



# 8. Exportar -----------------------------------------------------------------------------------------------------------------------------

# Export
write.table(dados_mun,   sf_mun, sep=';',row.names=F)
write.table(dados_reg,   sf_reg, sep=';',row.names=F)
write.table(dados_macro, sf_macro,sep=';',row.names=F)
write.table(dados_uf,    sf_state,sep=';',row.names=F)
write.table(dados_br,    sf_brazil,sep=';',row.names=F)


# FIM -----------------------------------------------------------------------------------------------------------------------------------------------