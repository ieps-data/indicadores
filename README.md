# IEPS Data
Este repositório contém os códigos usados para construir os indicadores disponibilizados pelo portal [IEPS Data](https://iepsdata.org.br/). Ele será atualizado sempre que um novo ano e/ou indicador for atualizado. O dicionário das variáveis pode ser encontrado [aqui](https://gitlab.com/ieps-data/indicadores/-/blob/main/codebook.csv).

Em caso de dúvidas, comentários ou sugestões, entre em contato através do e-mail iepsdata@ieps.org.br

Citação Sugerida: "Instituto de Estudos para Políticas de Saúde. *IEPS Data*. https://iepsdata.org.br/"

A estrutura básica do repositório divide-se em: 

* [01_tratar_indicadores](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores) : códigos de tratamento dos dados brutos, subdivididos nos blocos descritos abaixo. Os códigos estão escritos nas linguagens dos softwares estatísticos R e Stata. Em alguns casos, os dados são baixados diretamente pelo código via scraping, enquanto em outras parte-se direto para o tratamento de dados baixados manualmente.


* [02_merge_indicadores](https://gitlab.com/ieps-data/indicadores/-/tree/main/02_merge_indicadores) : constrói a base final ao nível do município, região de saúde, macrorregião de saúde, estado e Brasil, fazendo merge dos indicadores tratados previamente.

# Organização de 01_tratar_indicadores

* A) [Atenção Primária](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/A_atencao_primaria)
* B) [Mortalidade e Morbidade](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/B_mortalidade_morbidade)
* C) [Recursos](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/C_recursos)
* D) [Saúde Suplementar](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/D_saude_suplementar)
* E) [Gastos](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/E_gastos) 
* F) [Indicadores Socioeconômicos](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/F_socioeconomicos)
* G) [Demografia](https://gitlab.com/ieps-data/indicadores/-/tree/main/01_tratar_indicadores/G_demografia)


## Atenção primária
Subdividida em três subpastas: Cobertura AB, Cobertura Vacinal e Pré-Natal. No caso da cobertura AB, existem dois códigos, um de cobertura de atenção básica e de equipes de saúde da família, e outro de tratamento do indicador de cobertura de equipes de agentes comunitários de saúde. Para vacinas, a divisão de arquivos é feita por unidade geográfica. O código de pré-natal é único.

## Mortalidade e Morbidade 
Subdividida em quatro subpastas: Mortalidade Infantil, Mortalidade, Hospitalizações, e Auxiliar. Mortalidade Infantil possui dois códigos de acordo com a unidade geográfica. Mortalidade e Hospitalizações possuem códigos únicos, mas utilizam os códigos de programas inclusos na pasta auxiliar.

## Recursos
Pasta de arquivo único sobre indicadores de recursos humanos e físicos.

## Saúde Suplementar
Subdividida em duas subpastas: Leitos Não-SUS e Cobertura ANS. Cada uma composta por códigos únicos. Cobertura ANS se refere a planos privados de saúde.

## Gastos
Possui três códigos: dois de tratamento para as unidades geográficas municipal e estadual, além de um código que deflaciona os indicadores.

## Socioeconômicos
Subdividida em seis subpastas: Ideb, Bolsa Família, IDHM, Censo, PIB, e Renda. O Ideb possui três códigos diferenciados pela unidade geográfica, além de um código relativo ao censo e um de ponderação. O Bolsa Família conta com um código de tratamento e um que deflaciona os indicadores. O IDHM é subdividido por unidade geográfica. O censo possui código único, assim como PIB e Renda per capita.

## Demografia
Possui dois arquivos: um tratando dados de população por faixas etárias e outro para dados de população por faixas etárias e sexo.
